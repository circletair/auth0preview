package org.ce.assessment.userdb;

import org.ce.assessment.limeapi.LimeCallException;
import org.ce.assessment.limeapi.LimeClient;
import org.ce.assessment.limeapi.Survey;
import org.ce.assessment.limeapi.question.ListRadioQuestion;
import org.ce.assessment.platform.Platform;
import org.ce.assessment.platform.report.Report;
import org.ce.assessment.platform.report.Sector;
import org.ce.assessment.platform.report.SectorInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OrganizationProfile extends Report {

    private final static Logger log = LoggerFactory.getLogger(OrganizationProfile.class);


    private final String token;
    private final Organization  organization;
    private Sector nameSector;

    public OrganizationProfile(Organization organization, String token) {
        super(new Survey(Platform.instance().configuration.profileSurveyId));
        this.organization = organization;
        this.token = token;
        nameSector = new Sector("Full trading name").withDefinition(f.q("ProfileTradeName"));
        withSector(nameSector);

        withSector(new Sector("Established").withDefinition(f.date("ProfileYearEst")));
        withSector(new Sector("Description").withDefinition(f.q("ProfileCompDesc")));

        ListRadioQuestion profileSector = f.radio("ProfileSector");
        ListRadioQuestion profileIndustry = f.radio("ProfileIndustry");

        withSector(new SectorInterface() {
            @Override
            public String getName() {
                return "Industry";
            }

            @Override
            public String value() {
                return profileSector + " > " + profileIndustry;
            }
        });

        withSector(new Sector("Turnover (EUR)").withDefinition(f.radio("ProfileTurnover")));
        withSector(new Sector("Number of employees").withDefinition(f.radio("ProfileEmployees")));

        try {
            loadForToken(token);
        } catch (LimeCallException e) {
            e.printStackTrace();
        }
    }

    public String getSurveyLink(boolean newProfile) {
        if (token == null) {
            log.error("getting organization ("+organization.id+") profile survey link with NULL token");
            return "javascript: alert('Something went wrong. Cannot get the link to the survey')'";
        }
        return LimeClient.LIME_URL_SSL + "/index.php/survey/index"
                + "/sid/" + Platform.instance().configuration.profileSurveyId
                + "/token/" + token
                + ((newProfile)?"/lang/en/newtest/Y":"");
    }

    public String getName() {
        if (isSurveyCompleted()) {
            return nameSector.value();
        }
        return null;
    }
}
