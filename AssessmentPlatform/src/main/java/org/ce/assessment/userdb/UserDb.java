package org.ce.assessment.userdb;

import org.ce.assessment.platform.Platform;

public class UserDb {

    private OrganizationAdmins organizationAdmins;
    private Organizations organizations;
    private Users users;

    private final Platform platform;

    public UserDb(Platform platform) {
        this.platform = platform;
    }

    public UserDb() {
        this(Platform.instance());
    }

    public Organizations getOrganizations() {
        if (organizations == null) {
            organizations = new Organizations(platform);
        }
        return organizations;
    }

    public Users getUsers() {
        if (users == null) {
            users = new Users(platform);
        }
        return users;
    }

    public OrganizationAdmins getOrganizationAdmins() {
        if (organizationAdmins == null) {
            organizationAdmins = new OrganizationAdmins(platform);
        }
        return organizationAdmins;
    }


}
