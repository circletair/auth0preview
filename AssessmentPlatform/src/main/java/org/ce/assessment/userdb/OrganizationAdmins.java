package org.ce.assessment.userdb;

import org.ce.assessment.platform.DataException;
import org.ce.assessment.platform.DbQuery;
import org.ce.assessment.platform.Platform;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.NamingException;
import java.sql.SQLException;
import java.util.List;

// TODO: move inside of Organization class
public class OrganizationAdmins extends Users {

    private final static Logger log = LoggerFactory.getLogger(OrganizationAdmins.class);

    private final static String NAME = "organization_admins";

    private final static String SQL = ""; // TODO: add create sql

    public OrganizationAdmins(Platform platform) {
        super(platform);
    }

    public OrganizationAdmins() {
        super(Platform.instance());
    }

    public boolean add(String orgId, String userId) {
        String sql = "insert into " + NAME + "(organization_id, user_id) values (" + orgId + ", " + userId + " )";
        try (DbQuery query = query(sql)) {
            query.execute();
        } catch (SQLException | NamingException e) {
            log.error("Could not add organization admin (org_id=" + orgId + ", user_id=" + userId + "). " + e.toString());
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean remove(String orgId, String userId) {
        String sql = "delete from " + NAME + " where organization_id = " + orgId + " and user_id = " + userId + ";";
        try (DbQuery query = query(sql)) {
            query.execute();
        } catch (SQLException | NamingException e) {
            log.error("Could not remove organization admin (org_id=" + orgId + ", user_id=" + userId + "). " + e.toString());
            e.printStackTrace();
            return false;
        }
        return true;
    }


    public List<User> getForOrganization(String organizationId) {
        String sql = "select * from " + Users.USERS_VIEW +
                " where is_organization_admin = true and organization_id = " + organizationId + ";";
        try {
            return getUsersWithQuery(sql);
        } catch (DataException e) {
            log.error("Could not get admins for organization id (" + organizationId + "). " + e.toString());
            e.printStackTrace();
        }
        return null;
    }
}
