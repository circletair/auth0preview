package org.ce.assessment.platform.api;

import org.ce.assessment.platform.invites.Invite;
import org.ce.assessment.platform.invites.Invites;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

@Path("invites")
public class InvitesApi extends AbstractApi {

    private final static Logger log = LoggerFactory.getLogger(InvitesApi.class);

    @Context
    private HttpServletRequest request;

    @POST
    @Path("{token}")
    public Response respondToInvite(@PathParam("token") String token, @FormParam("action") String action) {

        Invite invite = new Invites().getByToken(token);
        if (invite == null) {
            String msg = "Unknown invite token " + token;
            log.error(msg);
            return Response.status(Response.Status.BAD_REQUEST).entity(msg).build();
        }

        boolean success = false;
        switch (action) {
            case A_ACCEPT:
                success = invite.accept();
                break;
            case A_REJECT:
                success = invite.reject();
                break;
            case A_RESEND:
                success = invite.sendInvitationEmails(getHost());
                break;
            default:
                log.error("Unknown invite action: " + action);
        }

        if (!success) {
            return Response.serverError().entity("Could not " + action + " the invite. Token: " + token).build();
        }
        return Response.ok().build();
    }

    @POST
    @Path("{token}/withdraw")
    public Response withdraw(@PathParam("token") String token) {

        log.debug("withdrawing invite with token " + token);

        Invites invites = new Invites();
        Invite invite = invites.getByToken(token);
        if (invite == null) {
            String msg = "Unknown invite token " + token;
            log.error(msg);
            return Response.status(Response.Status.BAD_REQUEST).entity(msg).build();
        }
//
//        User user = null;
//        try {
//            user = new AuthManager().getAuthedUser(request);
//        } catch (ServletException | IOException e) {
//            log.debug(e.toString());
//            return Response.serverError().entity(e.toString()).build();
//        }
//
//        if (user == null || !user.canAdminOrganization(invite.getOrganizationId())) {
//            log.error("Unauthorized user " + ((user == null)?"null":user.getEmail()) + " tried to withdraw invitation");
//            return Response.status(Response.Status.UNAUTHORIZED).build();
//        } else {
//            log.error("unknown user");
//        }

        log.info("withdrawing invitation (" + token + ", " + invite.getEmail() + ")"); // by " + user.getEmail());
        if (!invites.setInviteStatus(token, Invites.InviteStatus.WITHDRAWN)) {
            String msg = "Could not revoke invite with token " + token;
            log.error(msg);
            return Response.serverError().entity(msg).build();
        }

        return Response.ok().build();
    }

    @Override
    protected Logger getLogger() {
        return log;
    }


}
