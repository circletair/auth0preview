package org.ce.assessment.platform.report;

import org.ce.assessment.limeapi.answeroptions.RadioAnswerOption;
import org.ce.assessment.limeapi.question.ArrayQuestion;

public class Strategy extends ScaleSector {

    public enum EngagementLevel {
        NOT_APPLICABLE(0, "Not applicable"),
        NO_DEVELOPMENT(1, "No development"),
        EXPLORATION(2, "Exploration"),
        PILOT(3, "Pilot"),
        PARTIAL_SCALE(4, "Partial Scale"),
        FULL_SCALE(5, "Full Scale"),
        UNKNOWN(-1, "Unknown");

        EngagementLevel(int value, String name) {
            this.value = value;
            this.name = name;
        }

        public final int value;
        public final String name;

        public static EngagementLevel getForAnswerCode(String answerCode) {
            if ("".equals(answerCode)) {
                return UNKNOWN;
            }
            return values()[Integer.valueOf(answerCode.substring(1))-1];
        }
    }

    public Strategy(String name, ArrayQuestion question) {
        super(name, question);
    }

    public EngagementLevel getEngagementLevel() {
        RadioAnswerOption answerOption = getAnswerOption();
        if (answerOption == null) {
            return EngagementLevel.UNKNOWN;
        }
        return  EngagementLevel.getForAnswerCode(answerOption.getSelectedValueCode());
    }

    public boolean isOpportunity() {
        EngagementLevel engagementLevel = getEngagementLevel();
        return (engagementLevel == EngagementLevel.NO_DEVELOPMENT || engagementLevel == EngagementLevel.EXPLORATION);
        // TODO: throw an exception if the strategy question is not loaded yet
    }
}
