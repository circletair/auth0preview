package org.ce.assessment.platform.report;

public interface SectorInterface {
    /***
     * An element of a report
     * allows for arbitrary value definition
     */

    String getName();
    String value();
}
