package org.ce.assessment.platform.report;

/**
 *
 * Survey requirements:
 *   All questions must be called according to the element of the survey
 *
 *   {report code}: code of the element. i.e.   Design / Collaborate / Bus /
 *   {section code}: code of the section: i.e.   Int for Internal, Cyc for Cyclability
 *
 *   Total Score: PCT{report code}Score                     i.e. PCTDesignScore
 *   Section selection question: {report code}Overview      i.e. DesignOverview
 *   Section score: {report code}{section code}Score        i.e. DesignWasteScore   // TODO: update
 *   Strategy questions:
 *      * scale: {report code}{section code}{number}        i.e. DesignWaste1
 *      * comment: {report code}{section code}{number}Ex    i.e. DesignWaste1Ex
 *   Key Challenges questions:
 *      * selection: {report code}{section code}Ch          i.e. DesignWasteCh
 *      * comment:  {report code}{section code}ChExp        i.e. DesignWasteChExp
 *
 */

import org.ce.assessment.limeapi.Survey;

public class SevenElementsReport extends Report {

    private final String reportCode;


    public SevenElementsReport(String reportCode, Survey survey) {
        super(survey);
        this.reportCode = reportCode;
        withScoreQuestion(f.q("PCT" + reportCode + "Score"));
    }

    public SevenElementsSection addSection(String name, String sectionCode, int orderNumber) {
        SevenElementsSection section =
                (SevenElementsSection) new SevenElementsSection(name, reportCode+sectionCode, survey)
                .withScoreQuestion(f.q("PCT"+reportCode+sectionCode + "Score"))
                .withCondition(f.mcqOption(reportCode+"Overview[SQ00"+orderNumber+"]", true));

        sections.put(name, section);
        return section;
    }
}
