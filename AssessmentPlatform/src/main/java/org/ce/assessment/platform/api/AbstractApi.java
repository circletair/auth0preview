package org.ce.assessment.platform.api;


import org.ce.assessment.platform.Platform;
import org.slf4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;

public abstract class AbstractApi {

    protected static final String MONKEYS = "Fixer monkeys have been notified. Try again later.";

    public static final String API_NAME = "api";
    public static final String API_USER_DB = "userDb";
    public static final String API_COMMUNITY = "community";
    public static final String API_AUTH = "auth";

    // Parameters
    public static final String P_ACTION = "action";
    public static final String P_ORG_TOKEN = "orgToken";
    public static final String P_ORG_ID = "orgId";
    public static final String P_EMAIL = "email";
    public static final String P_NAME = "name";
    public static final String P_ORG_NAME = "orgName";
    public static final String P_USER_ID = "userId";
    public static final String P_LAST_NAME = "lastName";
    public static final String P_PHONE = "phone";
    public static final String P_JOB_TITLE = "jobTitle";
    public static final String P_CURRENT_PASSWORD = "currentPassword";
    public static final String P_PASSWORD = "password";
    public static final String P_PASSWORD2 = "password2";
    public static final String P_COMMUNITY_ID = "communityId";
    public static final String P_INVITE_TOKEN = "inviteToken";
    public static final String P_JOIN_COMMUNITY = "joinCommunity";
    public static final String P_INVITE_TYPE = "inviteType";

    // Actions
    // Auth
    public static final String A_SIGN_IN = "signIn";
    public static final String A_SIGN_OUT = "signOut";

    // User db
    public static final String A_INVITE_USER = "inviteUser";
    public static final String A_REGISTER = "register";
    public static final String A_REGISTER_BY_INVITATION = "registerByInvitation";

    public static final String A_WITHDRAW_INVITE = "withdrawInvite";
    public static final String A_MAKE_ADMIN = "makeAdmin";
    public static final String A_REVOKE_ADMIN = "revokeAdmin";
    public static final String A_CHANGE_PASSWORD = "changePassword";

    public static final String A_GET_ORGS_LIKE = "getOrgsLike";

    // Community
    public static final String V_INVITE_EXISTING = "inviteExisting";
    public static final String V_INVITE_BY_EMAIL = "inviteByEmail";


    // Invites
    public static final String A_ACCEPT = "accept";
    public static final String A_REJECT = "reject";
    public static final String A_RESEND = "resend";




    @Context protected HttpServletRequest request;
    @Context protected HttpServletResponse response;

    protected String getHost() {
        String host = "https://circle-lab.com/assessment/";
//        String host = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";
        getLogger().debug(host);
        return host;
    }

    protected final Platform platform = Platform.instance();

    protected String param(String param, String value) {
        return param + "="  + value + "&";
    }

    protected abstract Logger getLogger();
}
