package org.ce.assessment.platform;

import org.slf4j.Logger;

import javax.naming.NamingException;
import java.sql.SQLException;

public abstract class DbTable {

    private final String dataSourceName;
    protected final Platform platform;

    public DbTable(Platform platform) {
        this.platform = platform;
        this.dataSourceName = platform.getConfiguration().getDataSourceName();
    }

    protected DbQuery query(String sql) {
        return new DbQuery(dataSourceName, sql);
    }

    protected String quotes(String s) {
        return "'" + s + "'";
    }

    protected String quotesAndComma(String s) {
        return quotes(s) + ", ";
    }

    String comma(String s) {
        return " , " + s;
    }

    protected void execute(String sql) throws DataException {
        try (DbQuery query = query(sql)) {
            query.execute();
        } catch (SQLException |  NamingException e) {
            throw new DataException("", e);
        }
    }
    
    protected static boolean createDbTable(String name, String sql, String dataSourceName, Logger log) {
        try (DbQuery query = new DbQuery(dataSourceName, sql)) {
            query.execute();
            log.info("Created " + name + " tableName");
            return true;
        } catch (SQLException | NamingException e) {
            log.error("Could not create " + name + " tableName. " + e.toString());
            e.printStackTrace();
        }

        return false;
    }
}
