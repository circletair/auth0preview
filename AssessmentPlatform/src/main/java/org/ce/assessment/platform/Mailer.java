package org.ce.assessment.platform;

import com.sparkpost.Client;
import com.sparkpost.exception.SparkPostException;
import com.sparkpost.model.AddressAttributes;
import com.sparkpost.model.RecipientAttributes;
import com.sparkpost.model.TemplateContentAttributes;
import com.sparkpost.model.TransmissionWithRecipientArray;
import com.sparkpost.model.responses.Response;
import com.sparkpost.resources.ResourceTransmissions;
import com.sparkpost.transport.IRestConnection;
import com.sparkpost.transport.RestConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;


public class Mailer extends Client {

    private final static Logger log = LoggerFactory.getLogger(Mailer.class);

    private final MailerConfiguration config;

    Mailer(MailerConfiguration config) {
        super(config.password);
        this.config = config;

    }
    
    public boolean sendMsg(String address, String subject, String msg) {

        TransmissionWithRecipientArray transmission = new TransmissionWithRecipientArray();

        RecipientAttributes recipientAttributes = new RecipientAttributes();
        recipientAttributes.setAddress(new AddressAttributes(address));
        List<RecipientAttributes> recipientArray = new ArrayList<>();
        recipientArray.add(recipientAttributes);
        transmission.setRecipientArray(recipientArray);

        TemplateContentAttributes contentAttributes = new TemplateContentAttributes();
        contentAttributes.setFrom(new AddressAttributes(config.fromAddress, config.fromName, ""));

        contentAttributes.setSubject(subject);
        contentAttributes.setHtml(msg);
        contentAttributes.setText(msg);
        transmission.setContentAttributes(contentAttributes);

        try {
            IRestConnection connection = new RestConnection(this, IRestConnection.SPC_US_ENDPOINT);
            Response response = ResourceTransmissions.create(connection, 0, transmission);

            if (response.getResponseCode() != 200) {
                log.error("Could not send email. Response code " + response.getResponseCode());
                return false;
            }
        } catch (SparkPostException e) {
            log.error("Could not send email do to server error. " + e.toString());
            e.printStackTrace();
            return false;
        }
        log.info("Successfully sent email to " + address);
        return true;
    }
}
