package org.ce.assessment.platform.report;

import org.ce.assessment.limeapi.answeroptions.RadioAnswerOption;
import org.ce.assessment.limeapi.question.ArrayQuestion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Function;

/**
 * Special kind of sector with a scale question used by Seven Elemenents survey. It is based on an ArrayQuestion
 * with only one row - that is how the surveys are implemented.
 */
public class ScaleSector extends Sector {

    private Logger log = LoggerFactory.getLogger(ScaleSector.class);

    private final ArrayQuestion question;


    private Function<RadioAnswerOption, Boolean> extraConditionFunction = radioAnswerOption -> true;

    public ScaleSector(String name, ArrayQuestion question)  {
        super(name);
        this.question = question;
    }

    public ScaleSector withExtraCondition(Function<RadioAnswerOption, Boolean> function) {
        this.extraConditionFunction = function;
        return this;
    }

    protected RadioAnswerOption getAnswerOption() {
        Object[] selectedOptions = question.getOptions().values().toArray();
        if (selectedOptions.length != 1) {
            log.error("ArrayQuestion(" + question.questionCode + ") returned more than one row");
            return null;
        }
        return (RadioAnswerOption) selectedOptions[0];
    }

    public String value() {
        RadioAnswerOption answerOption = getAnswerOption();
        if (answerOption == null) {
            return "error";
        }
        return answerOption.getSelectedValue();
    }

    @Override
    public boolean isEnabled() {
        return super.isEnabled() && extraConditionFunction.apply(getAnswerOption());
    }
}
