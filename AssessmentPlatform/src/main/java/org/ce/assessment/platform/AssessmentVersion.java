package org.ce.assessment.platform;

import org.ce.assessment.platform.report.Section;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class AssessmentVersion {

    String id;
    String name;
    private final Map<String, AssessmentVersionSection> sections = new LinkedHashMap<>();
    private boolean isActivated;


    public AssessmentVersion(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AssessmentVersion addSection(AssessmentVersionSection section) {
        sections.put(section.getId(), section);
        return this;
    }

    AssessmentVersion withSections(List<AssessmentVersionSection> sections) {
        for (AssessmentVersionSection section : sections) {
            this.sections.put(section.getId(), section);
        }
        return this;
    }

    public List<AssessmentVersionSection> getSectionList() {
        return new ArrayList<>(sections.values());
    }

    public Map<String, AssessmentVersionSection> getSections() {
        return sections;
    }

    public boolean isActivated() {
        return isActivated;
    }

    AssessmentVersion setActivated(boolean isActivated) {
        this.isActivated = isActivated;
        return this;
    }

    public String getNextSectionId(String sectionId) {
        // TODO: only works for less than 10 report sections
        int i = Integer.valueOf(sectionId) % 10;
        if (i == sections.size()) {
            return id + "1";
        }
        return id + (++i);
    }

    public String getPrevSectionId(String sectionId) {
        // TODO: only works for less than 10 report sections
        int i = Integer.valueOf(sectionId) % 10;
        if (i == 1) {
            return id + sections.size();
        }
        return id + (--i);
    }

    public Map<String, List<String>> getTiers() {
        Map<String, List<String>> tiers = new LinkedHashMap<>();

        for (AssessmentVersionSection t1 : sections.values()) {
            List<String> t2Elements = new ArrayList<>();
            for (Section t2 : t1.prepareReport().sections.values()) {
                t2Elements.add(t2.getName());
            }
            tiers.put(t1.getName(), t2Elements);
        }
        return tiers;
    }

    @Override
    public String toString() {
        return name + "(" + id + ")";
    }
}
