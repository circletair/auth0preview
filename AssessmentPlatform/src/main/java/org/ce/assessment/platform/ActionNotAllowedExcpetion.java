package org.ce.assessment.platform;

public class ActionNotAllowedExcpetion extends Exception {

	private static final long serialVersionUID = 1L;
	
	public ActionNotAllowedExcpetion(String msg) {
		super(msg);
	}
}
