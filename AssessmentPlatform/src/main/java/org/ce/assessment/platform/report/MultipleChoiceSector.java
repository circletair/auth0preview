package org.ce.assessment.platform.report;


import org.ce.assessment.limeapi.answeroptions.CheckedAnswerOption;
import org.ce.assessment.limeapi.question.MultipleChoiceQuestion;
import org.ce.assessment.limeapi.question.TextQuestion;

import java.util.List;

public class MultipleChoiceSector extends Sector {

    private final MultipleChoiceQuestion optionsQuestion;
    private TextQuestion explanationQuestion;
    private boolean showUnselected = true;


    public MultipleChoiceSector(String name, MultipleChoiceQuestion optionsQuestion) {
        super(name);
        this.optionsQuestion = optionsQuestion;
    }

    MultipleChoiceSector withExplanation(TextQuestion explanationQuestion) {
        this.explanationQuestion = explanationQuestion;
        return this;
    }

    MultipleChoiceSector hideUnselected() {
        showUnselected = false;
        return this;
    }
    public boolean showUnselected() {
        return showUnselected;
    }

    public List<CheckedAnswerOption> getSelectedOptions() {
        return optionsQuestion.getSelectedOptions();
    }

    public List<CheckedAnswerOption> getUnselectedOptions() {
        return optionsQuestion.getUnselectedOptions();
    }

    public boolean hasExplanation() {
        return explanationQuestion != null;
    }
    public String getExplanation() {
        if (explanationQuestion != null) {
            return explanationQuestion.value();
        }
        return "";
    }
}
