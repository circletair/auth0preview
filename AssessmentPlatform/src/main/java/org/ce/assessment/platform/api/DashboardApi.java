package org.ce.assessment.platform.api;

import org.ce.assessment.platform.CommunityAssessments;
import org.ce.assessment.platform.dashboard.Challenges;
import org.ce.assessment.platform.dashboard.Opportunities;
import org.ce.assessment.platform.dashboard.Scores;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;


// TODO: refactor : add communityAssessmentId to class path
@Path("dashboard/{communityAssessmentId}")
public class DashboardApi extends AbstractApi {
    private final static Logger log = LoggerFactory.getLogger(DashboardApi.class);


    private final String communityAssessmentId;

    public DashboardApi(@PathParam("communityAssessmentId") String communityAssessmentId) {
        this.communityAssessmentId = communityAssessmentId;
    }

    @Override
    protected Logger getLogger() {
        return log;
    }

    @POST
    @Path("update")
    public Response createCommunity() {
        final boolean success = new Opportunities().captureForCommunity(communityAssessmentId);
        if (success) {
            return Response.ok().build();
        }
        return Response.serverError().entity("could not update opportunities due to a server error").build();
    }

    @GET
    @Path("challenges")
    public String getChallenges() {
        return new Challenges().getForCommunity(communityAssessmentId);
    }

    @GET
    @Path("minMaxAvg")
    public String getMinMaxAvg() {
        return new Scores().getTotalsForCommunityAssessment(communityAssessmentId);
    }

    @GET
    @Path("minMaxAvg/ownAssessment/{assessmentId}")
    public String getMinMaxAvgWithAssessment(@PathParam("assessmentId") String assessmentId) {
        return new Scores().getTotalsForCommunityAssessment(communityAssessmentId, assessmentId);
    }

    @GET
    @Path("opportunities")
    public String getOpportunities() {
        return new Opportunities().getForCommunity(communityAssessmentId);
    }

    @GET
    @Path("nOrganizations")
    public int getNOrganizations() {
        return new CommunityAssessments().getNOrganizationInDashboard(communityAssessmentId);
    }
}
