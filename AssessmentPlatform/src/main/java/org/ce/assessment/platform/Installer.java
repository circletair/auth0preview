package org.ce.assessment.platform;

import org.ce.assessment.userdb.*;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class  Installer {

    protected final Platform platform;
    protected final String dataSourceName;


    public Installer() {
        this.platform = Platform.instance();
        this.dataSourceName = platform.configuration.getDataSourceName();
    }

	public boolean install() {
		boolean success = Users.createDbTable(dataSourceName);
		success &= PasswordResetRequests.createDbTable(dataSourceName);
		return success;
	}
	
	public int importTokens(String surveyId) {
		int nTokens = 0;

        DataSource ds;
        try {
            InitialContext cxt = new InitialContext();
            ds = (DataSource) cxt.lookup("java:/comp/env/" + dataSourceName);
        } catch (NamingException e) {
            System.out.println("Could not query tokens. Could not get dataSource from context");
            e.printStackTrace();
            return -1;
        }

        try (
            DbQuery selectQuery = new DbQuery(Platform.LIME_DATA_SOURCE_NAME, "select token from lime_tokens_" + surveyId + ";");
            DbQuery deleteQuery = new DbQuery(dataSourceName, "delete from assessments;");
            Connection con = ds.getConnection();
            PreparedStatement insertQuery = con.prepareStatement("insert into assessments (token) values (?);")
        ) {
            deleteQuery.execute();

			ResultSet results = selectQuery.getResults();
			String token;
			while (results.next()) {
                token = results.getString("token");
                insertQuery.setString(1, token);
                insertQuery.addBatch();
                ++nTokens;
            }
            insertQuery.executeBatch();
		} catch (SQLException | NamingException e) {
			System.out.println("Could not insert tokens.");
			e.printStackTrace();
			return -1;
		}
		
		return nTokens;
	}

	public boolean createAdminUser(String adminEmail) {
        Organization org  = new Organizations().createNew("Circle Economy");
        Users users = platform.getUserDb().getUsers();
        User user = users.createNew(adminEmail, org.id);
        user.setPassword("pop");
        users.update(user);
        new Admins(platform).addAdmin(user);
        return true;
    }



}
