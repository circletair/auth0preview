package org.ce.assessment.platform.dashboard;

public class Challenge {
    public final String name;
    public final String tier2Strategy;
    public final String element;
    public final String elementId;
    public final int frequency;


    public Challenge(String name, String tier2Strategy, String element, String elementId, int frequency) {
        this.name = name;
        this.tier2Strategy = tier2Strategy;
        this.element = element;
        this.elementId = elementId;
        this.frequency = frequency;
    }
}
