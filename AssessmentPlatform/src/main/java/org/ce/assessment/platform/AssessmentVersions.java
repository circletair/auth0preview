package org.ce.assessment.platform;

import java.util.List;

public interface AssessmentVersions {

    List<AssessmentVersion> getAll();
    List<AssessmentVersion> getAll(String params);
    AssessmentVersion getById(String id);
    AssessmentVersion create(String name);
    boolean delete(AssessmentVersion assessmentVersion);

}
