package org.ce.assessment.platform.api;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.ce.assessment.platform.AuthManager;
import org.ce.assessment.platform.invites.Invite;
import org.ce.assessment.platform.invites.Invites;
import org.ce.assessment.userdb.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;

@Path("organizations")
public class OrganizationsApi extends AbstractApi {

    private final static Logger log = LoggerFactory.getLogger(OrganizationsApi.class);

    @Context private HttpServletRequest request;
    @Context private HttpServletResponse response;

    private AuthManager authManager = new AuthManager();

    @GET
    public String getOrganizations(@QueryParam("query") String query) {
//        if (authManager.getAuthedUser(request) == null) {
//            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
//            return "{}";
//        }
        // TODO: add auth check

        JsonArray jsonList = new JsonArray();

        final List<Organization> organizationsLike =
                (query != null)
                        ? new Organizations().getLike(query)
                        : new Organizations().getAll();

        for (Organization o : organizationsLike) {
            JsonObject orgJson = new JsonObject();
            orgJson.addProperty("id", o.id);
            orgJson.addProperty("name", o.getName());
            jsonList.add(orgJson);
        }

        JsonObject resultJson = new JsonObject();
        resultJson.add("organizations", jsonList);
        return resultJson.toString();
    }


    @POST @Path("{id}/users/invite")
    public Response inviteUser(
                    @PathParam("id") String orgId,
                    @FormParam("email") String email,
                    @DefaultValue("false") @FormParam("admin") String asAdmin)
            throws ServletException, IOException
    {
        if (email == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity("invalid email").build();
        }

        // Check if email is already registered or invited
        if (null != new Users().getByEmail(email)) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity("user with email " + email + " is already registered").build();
        }

        Invites invites = new Invites();

        if (null != invites.getActiveByEmail(email)) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity("user with email " + email + " is already invited").build();
        }

        final Organization organization = new Organizations().getById(orgId);
        if (organization == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity("unknown organization").build();
        }

        User authedUser = authManager.getAuthedUser(request);
        if (!authedUser.canAdminOrganization(organization)) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(
                    "User has no admin rights to invite other users to the organizations").build();
        }

        // Create invite and send an email
        Invite invite = invites.newOrganizationByEmail(organization, email, Boolean.valueOf(asAdmin));
        if (invite == null) {
            String msg = "could not create invite for " +email;
            log.error(msg);
            return Response.serverError().entity(msg).build();
        }

        if (!invite.sendInvitationEmails(getHost()))
        {
            invites.deleteInvite(invite.token);
            String msg = "Could not send invitation email to " +email;
            log.error(msg);
            return Response.serverError().entity(msg).build();
        }

        return Response.ok().build();
    }

    @POST @Path("{id}/admins/{userId}")
    public Response addAdmin(@PathParam("id") String orgId, @PathParam("userId") String userId) {

//        if (!authManager.getAuthedUser(request).canAdminOrganization()) {
//            return Response.status(Response.Status.UNAUTHORIZED).build(); // TODO: react on front end side
//        }

        boolean success = new OrganizationAdmins().add(orgId, userId);
        if (!success) {
            String msg = "could not add admin user";
            log.error(msg);
            return Response.serverError().entity(msg).build();
        }
        return Response.ok().build();
    }

    @POST @Path("{id}/admins/{userId}/revoke")
    public Response revokeAdmin(@PathParam("id") String orgId, @PathParam("userId") String userId) {

//        if (!authManager.getAuthedUser(request).canAdminOrganization()) {
//            return Response.status(Response.Status.UNAUTHORIZED).build(); // TODO: react on front end side
//        }

        boolean success = new OrganizationAdmins().remove(orgId, userId);
        if (!success) {
            String msg = "could not remove admin user";
            log.error(msg);
            return Response.serverError().entity(msg).build();
        }
        return Response.ok().build();
    }

    @POST @Path("{id}/update-name")
    public Response updateName(@DefaultValue("") @PathParam("id") String id) {
        Organizations organizations = new Organizations();
        Organization organization = organizations.getById(id);
        if (organization == null) {
            log.error("unknown organization id " + id);
            return Response.status(Response.Status.BAD_REQUEST).entity("unknown organization id").build();
        }

        String name = organization.getProfile().getName();
        if (name != null) {
            organization.setName(name);
            boolean updated = organizations.update(organization);

            if (!updated) {
                String msg = "Could not update organization(" + id + ") due to a server error.";
                log.error(msg);
                return Response.serverError().entity(msg).build();
            }
        }
        return Response.ok().build();
    }

    @Override
    protected Logger getLogger() {
        return log;
    }
}
