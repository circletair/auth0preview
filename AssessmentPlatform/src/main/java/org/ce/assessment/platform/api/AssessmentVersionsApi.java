package org.ce.assessment.platform.api;

import com.google.gson.Gson;
import org.ce.assessment.platform.AssessmentVersion;
import org.ce.assessment.platform.AssessmentVersionSection;
import org.ce.assessment.platform.AssessmentVersionSectionsDb;
import org.ce.assessment.platform.AssessmentVersionsDb;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;

@Path("assessment-versions")
public class AssessmentVersionsApi {

    public final AssessmentVersionsDb V = new AssessmentVersionsDb();
    public final AssessmentVersionSectionsDb S = new AssessmentVersionSectionsDb();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getAll() {
        return new Gson().toJson(V.getAll());
    }

    @GET @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getById(@PathParam("id") String id) {
        return new Gson().toJson(V.getById(id));
    }

    @POST
    @Path("create")
    public Response createVersion(@FormParam("name") String name, @Context UriInfo uriInfo) {
        if (name != null) {
            AssessmentVersion assessmentVersion = V.create(name); // TODO: throw an exception?
            if (assessmentVersion != null) {
                URI uri = uriInfo.getAbsolutePathBuilder().path(assessmentVersion.getId()).build();
                return Response.created(uri).build();
            } else {
                return Response.serverError().build();
            }
        } else {
            return Response.serverError().build(); //TODO: better error message status
        }
    }

    @POST @Path("{id}")
    public Response update(
            @PathParam("id") String id,
            @FormParam("activate") String activate) {
        AssessmentVersion v = V.getById(id);
        if (Boolean.valueOf(activate)) {
            if (V.activate(v)) {
                return Response.ok().build();
            } else {
                return Response.serverError().build();
            }
        }
        return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
    }

    @POST
    @Path("{id}/delete")
    public Response deleteVersion(@PathParam("id") String id) {
        if (id != null) {
            if (V.delete(id)) {
                return Response.ok().build();
            } else {
                return Response.serverError().build();
            }
        }
        else {
            return Response.status(Response.Status.BAD_REQUEST).entity("missing assessment version id").build();
        }
    }

    @POST @Path("{id}/sections")
    public Response createSection(
            @PathParam("id") String assessmentVersionId,
            @FormParam("kind") String kind,
            @FormParam("name") String name,
            @DefaultValue("") @FormParam("surveyId") String surveyId,
            @Context UriInfo uriInfo
    ) {
        if (!canEditAssessmentVersion(assessmentVersionId)) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        AssessmentVersionSection assessmentVersionSection = S.create(assessmentVersionId, kind, name, surveyId);
        if (assessmentVersionSection == null) {
            return Response.serverError().build();
        }
        URI uri = uriInfo.getAbsolutePathBuilder()
                .path(assessmentVersionId).path("sections").path(assessmentVersionSection.getId()).build();
        return Response.created(uri).build();
    }

    @POST @Path("{versionId}/sections/{sectionId}/delete")
    public Response deleteSection(
            @PathParam("versionId") String assessmentVersionId,
            @PathParam("sectionId") String sectionId
    ) {
        if (!canEditAssessmentVersion(assessmentVersionId)) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        final boolean success = S.delete(sectionId);
        if (success) {
            return Response.ok().build();
        } else {
            return Response.serverError().build();
        }
    }

    private boolean canEditAssessmentVersion(String id) {
        AssessmentVersion v = V.getById(id);
        return (v != null && !v.isActivated());
    }

}
