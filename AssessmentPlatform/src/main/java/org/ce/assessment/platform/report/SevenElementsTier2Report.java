package org.ce.assessment.platform.report;

import org.ce.assessment.limeapi.Survey;

public class SevenElementsTier2Report extends SevenElementsReport {

    public SevenElementsTier2Report(String reportCode, Survey survey) {
        super(reportCode, survey);
    }

    @Override
    public SevenElementsSection addSection(String name, String sectionCode, int orderNumber) {
        return super.addSection(name, sectionCode, orderNumber)
                .withStrategy(name, 1)
                .withKeyChallenges()
                .withKeyOpportunities()
                .withNotes()
                ;
    }
}
