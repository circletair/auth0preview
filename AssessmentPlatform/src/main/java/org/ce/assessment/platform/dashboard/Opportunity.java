package org.ce.assessment.platform.dashboard;

public class Opportunity {

    public final String name;
    public final String element;
    public final String elementId;
    public final int frequency;


    public Opportunity(String name, String element, String elementId, int frequency) {
        this.name = name;
        this.element = element;
        this.elementId = elementId;
        this.frequency = frequency;
    }
}
