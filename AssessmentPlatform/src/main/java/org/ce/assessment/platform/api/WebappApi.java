package org.ce.assessment.platform.api;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;



@Path("webapp")
public class WebappApi {

    public static final String COMMUNITY_CUR_TAB = "communityCurTab";
    public static final String ORGANIZATION_CUR_TAB = "organizationCurTab";

    public enum Tab {
        DASHBOARD,
        MEMBERS,
        TEAM,
        ASSESSMENTS,
        COMMUNITIES,
        PROFILE,
        SETTINGS,
        WASTE,
        UNKNOWN;
    }

    @Context HttpServletRequest request;

    @POST @Path("community-tabs/current")
    public Response setCommunityTab(@FormParam("tab") String tabName) {
        return setTab(tabName, COMMUNITY_CUR_TAB);
    }

    @POST @Path("organization-tabs/current")
    public Response setOrganizationTab(@FormParam("tab") String tabName) {
        return setTab(tabName, ORGANIZATION_CUR_TAB);
    }

    private Response setTab(@FormParam("tab") String tabName, String page) {
        try {
            Tab tab = Tab.valueOf(tabName);
            request.getSession().setAttribute(page, tab);
            return Response.ok().build();
        } catch (IllegalArgumentException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Unknown tab " + tabName).build();
        }
    }
}
