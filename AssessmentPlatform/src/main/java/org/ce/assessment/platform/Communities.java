package org.ce.assessment.platform;

import org.ce.assessment.userdb.Organization;
import org.ce.assessment.userdb.Organizations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.NamingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Communities extends DbTable {

    private final static Logger log = LoggerFactory.getLogger(Communities.class);

    public Communities(Platform platform) {
        super(platform);
    }

    public Communities() {
        super(Platform.instance());
    }

    private static final String COMMUNITIES = "communities";
    private static final String LIST_VIEW = "communities_list_view";

    private Community fromDbRow(ResultSet rs) throws SQLException {
        final String communityId = rs.getString("id");
        final String name = rs.getString("name");
        Community community = new Community(
                communityId,
                name,
                new CommunityMembers(platform, communityId)
        );

        final String adminOrgId = rs.getString("admin_organization_id");
        final Organization adminOrg = (adminOrgId == null)?null:new Organizations(platform).getById(adminOrgId);
        community.setAdminOrganization(adminOrg);

        return community;
    }

    public List<Community> getCommunitiesList() {
        List<Community> communities = new ArrayList<>();

        String sql = "select * from " + LIST_VIEW + ";";
        try (final DbQuery query = query(sql);
             final ResultSet rs = query.getResults())
        {
            while (rs.next()) {
                Community community = fromDbRow(rs)
                        .setNMembers(rs.getInt("n_members"));
                communities.add(community);
            }
        } catch (SQLException | NamingException e) {
            log.error("Could not get communities list. " + e.toString());
        }

        return communities;
    }

    private Community getBy(String args) {
        String sql = "select * from " + COMMUNITIES + " where " + args + ";";
        Community community = null;
        try (DbQuery q = query(sql)) {
            ResultSet rs = q.getResults();
            if (rs.next()) {
                community = fromDbRow(rs);
            } else {
                log.warn("Could not find community by " + args);
            }
        } catch (SQLException | NamingException e) {
            log.error("Could not get community by " + args + ". " + e.toString());
        }
        return community;
    }

    // TODO: return result with error
    public boolean create(String name) {
        String sql = "insert into " + COMMUNITIES + " (name) values ('" + name + "');";
        try (DbQuery insertQuery = query(sql)) {
            insertQuery.execute();
        } catch (SQLException | NamingException e) {
            log.error("Could not create community " + name + ". " + e.toString());
            return false;
        }
        log.info("Create community '" + name + "'");
        return true;
    }


    public Community getById(String id) {
        return getBy("id="+id);
    }

    public boolean setAdminOrganization(String communityId, String adminId) {
        final Organization organization = platform.getUserDb().getOrganizations().getById(adminId);
        if (organization == null) {
            return false;
        }
        return setAdminOrganization(communityId, organization);
    }

    private boolean setAdminOrganization(String communityId, Organization adminOrganization) {
        final Community community = getById(communityId);
        if (community == null) {
            return false;
        }
        community.setAdminOrganization(adminOrganization);
        return update(community);
    }


    public boolean update(Community community) {

        String sql = "update " + COMMUNITIES + " set " +
                " admin_organization_id = " + community.getAdminOrganization().id+
                " , name = " + quotes(community.getName())+
                " where id = " + community.id + ";";

        try (DbQuery updateQuery = query(sql)) {
            updateQuery.execute();
        } catch (SQLException | NamingException e) {
            log.error("Could not update " + community.toString() + "; " + e.toString());
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public List<Community> getWithOrganization(String organizationId) {
        String sql = "select C.* " +
                " from " +
                "       communities C " +
                "       inner join community_members M on C.id = M.community_id " +
                " where M.organization_id = " + organizationId + ";";
        List<Community> communities = new ArrayList<>();
        try (DbQuery q = query(sql);
            ResultSet rs = q.getResults())
        {
            while (rs.next()) {
                communities.add(fromDbRow(rs));
            }
        } catch (SQLException | NamingException e) {
            log.error("Error getting communities with organization " + organizationId);
            e.printStackTrace();
            return null;
        }
        return communities;
    }

    public Community getByAdminOrganizationId(String organizationId) {
        return getBy("admin_organization_id="+organizationId);
    }

}
