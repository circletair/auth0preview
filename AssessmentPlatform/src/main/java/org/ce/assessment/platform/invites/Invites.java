package org.ce.assessment.platform.invites;

import org.ce.assessment.platform.*;
import org.ce.assessment.userdb.Organization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.NamingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Invites extends DbTable {


    public Invites() {
        super(Platform.instance());
    }

    public enum InviteStatus {
        PENDING("pending"),
        WITHDRAWN("withdrawn"),
        ACCEPTED("accepted"),
        REJECTED("rejected"),

        UNKNOWN("unknown");

        public final String sqlValue;

        InviteStatus(String sqlValue) {
            this.sqlValue = sqlValue;
        }

        public static InviteStatus fromString(String string) {
            for (InviteStatus s : InviteStatus.values()) {
                if (s.sqlValue.equals(string)) {
                    return s;
                }
            }
            return UNKNOWN;
        }

        @Override
        public String toString() {
            return sqlValue;
        }
    }

    public enum InviteType {
        USER("user"),
        ORGANIZATION_BY_COMMUNITY("organization_by_community"),
        COMMUNITY_BY_EMAIL("community_by_email"),
        COMMUNITY_BY_ORG("community_by_org"),
        UNKNOWN("unknown")
        ;

        public final String sqlValue;

        InviteType(String sqlValue) {
            this.sqlValue = sqlValue;
        }

        public static InviteType fromString(String string) {

            for (InviteType t: InviteType.values()) {
                if (t.sqlValue.equals(string)) {
                    return t;
                }
            }
            return UNKNOWN;
        }

        @Override
        public String toString() {
            return sqlValue;
        }
    }


    // TODO: add sql and create tableName

    public static final String NAME = "invites";
    private final static Logger log = LoggerFactory.getLogger(Invites.class);

    public Invites(Platform platform) {
        super(platform);
    }

    public CommunityMemberByEmailInvite newCommunityByEmail(String communityId, String email) {
        Invite invite = new CommunityMemberByEmailInvite(generateToken(), communityId, email);
        return  (CommunityMemberByEmailInvite) createInDb(invite);
    }

    public CommunityAdminByEmailInvite newCommunityAdminByEmail(String communityId, String email) {
        Invite invite = new CommunityAdminByEmailInvite(generateToken(), communityId, email);
        return (CommunityAdminByEmailInvite) createInDb(invite);
    }

    public CommunityMemberExistingOrgInvite newCommunityByOrg(String communityId, Organization organization) {
        Invite invite = new CommunityMemberExistingOrgInvite(generateToken(), communityId, organization.id);
        return  (CommunityMemberExistingOrgInvite) createInDb(invite);
    }

    public OrganizationMemberInvite newOrganizationByEmail(Organization organization, String email, boolean asAdmin) {
        Invite invite = new OrganizationMemberInvite(generateToken(), organization.id, email, asAdmin);
        return (OrganizationMemberInvite) createInDb(invite);
    }

    public OrganizationMemberInvite newOrganizationAdminByEmail(Organization organization, String email) {
        Invite invite = new OrganizationMemberInvite(generateToken(), organization, email, true);
        return (OrganizationMemberInvite) createInDb(invite);
    }

    public OrganizationByCommunityInvite newOrganizationByCommunity(Community community, Organization organization, String email) {
        Invite invite = new OrganizationByCommunityInvite(generateToken(), community, organization, email);
        return (OrganizationByCommunityInvite) createInDb(invite);
    }

    public Invite getActiveByEmail(String email) {
        if (email == null) {
            log.error("Email is null. " + Thread.currentThread().getStackTrace());
            return null;
        }

        try (DbQuery query = query(
                "select * from " + NAME + " where status='" + InviteStatus.PENDING + "' and email='" + email + "';");
             ResultSet rs = query.getResults()) {
            if (rs.next()) {
                return fromDbRow(rs);
            }
        }
        catch (SQLException | NamingException e) {
            log.error("Could not retrieve an active invite by email " + email);
            e.printStackTrace();
            return null;
        }
        return null;
    }

    public Invite getByToken(String token) {
        if (token == null) {
            log.error("Token is null. " + Thread.currentThread().getStackTrace());
            return null;
        }

        try (DbQuery query = query("select * from " + NAME + " where token='" + token + "';")) {
            ResultSet rs = query.getResults();
            if (rs.next()) {
                return fromDbRow(rs);
            }
        }
        catch (SQLException | NamingException e) {
            log.error("Could not retrieve an invite by token " + token);
            e.printStackTrace();
            return null;
        }
        return null;
    }

    public List<Invite> getCommunityMemberInvites(String communityId) {
        String sql = "select * from " + NAME + " where " +
                " type > '" + InviteType.USER.toString() + "' " +
                " and community_id = " + communityId + " " +
                " and as_admin = false " +
                " and status = '" + InviteStatus.PENDING.toString() + "';";
        try {
            return getInvitesWithSql(sql);
        } catch (DataException e) {
            log.error("Could not get a list of active member invitations for community id: " + communityId
                    + ". " + e.toString());
            e.printStackTrace();
            return null;
        }
    }

    public List<Invite> getCommunityAdminInvite(String communityId) {
        String sql = "select * from " + NAME + " where "
                + " type = '" + InviteType.COMMUNITY_BY_EMAIL + "' "
                + " and community_id = " + communityId
                + " and as_admin = true "
                + " and status = '" + InviteStatus.PENDING.toString() + "';";

        try {
            return  getInvitesWithSql(sql);
        } catch (DataException e) {
            log.error("Could not get a list of active admin invitations for community id: " + communityId
                    + ". " + e.toString());
            e.printStackTrace();
            return null;
        }
    }

    public List<Invite> getCommunityInvitesForOrganization(String organizationId) {
        String sql = "select * from " + NAME + " where " +
                        "organization_id = " + organizationId +
                        " and type = '" + InviteType.COMMUNITY_BY_ORG.toString() + "'" +
                        " and status = '" + InviteStatus.PENDING + "';";
        try {
            return getInvitesWithSql(sql);
        } catch (DataException e) {
            log.error("Could not get community invites for organization (" + organizationId + ". " + e.toString());
            e.printStackTrace();
            return null;
        }
    }

    public List<Invite> getUserInvitesForOrganization(Organization organization) {
        String sql = "select * from " + NAME + " where " +
                "organization_id = " + organization.id +
                " and (type = '" + InviteType.USER + "' OR type = '" + InviteType.ORGANIZATION_BY_COMMUNITY + "') " +
                " and status = '" + InviteStatus.PENDING + "';";
        try {
            return  getInvitesWithSql(sql);
        } catch (DataException e) {
            log.error("Could not get user invites for organization " + organization.getName() + ". " + e.toString());
            e.printStackTrace();
            return null;
        }
    }

    private List<Invite> getInvitesWithSql(String sql) throws DataException {
        List<Invite> invites = new ArrayList<>();

        try (DbQuery query = query(sql);
             ResultSet rs = query.getResults())
        {
            while (rs.next()) {
                invites.add(fromDbRow(rs));
            }
        } catch (NamingException | SQLException e) {
            throw new DataException(e);
        }
        return invites;
    }

    // TODO: change to accept/reject/revoke
    public boolean setInviteStatus(String token, InviteStatus status) {
        String sql =
                "update " + NAME
                + " set status='" + status.toString() + "', date_completed = now() "
                + " where token = '" + token + "'";
        try (DbQuery query = query(sql)) {
            query.execute();
        } catch (SQLException | NamingException e) {
            log.error("Could not update status (" + status.toString() + ") of an invitation with token "
                    + token + ". " + e.toString());
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean deleteInvite(String token) {
        String sql = "delete from " + NAME + " where token = '" + token + "';";
        try {
            execute(sql);
        }
        catch (DataException e) {
            log.error("Could not delete invite with token: " + token + ". " + e.toString());
            return false;
        }
        return true;
    }


    // TODO: update
    private Invite fromDbRow(ResultSet rs) throws SQLException {
        String token = rs.getString("token");
        InviteType type = InviteType.fromString(rs.getString("type"));
        InviteStatus status = InviteStatus.fromString(rs.getString("status"));
        boolean asAdmin = rs.getBoolean("as_admin");
        String email = rs.getString("email");
        String organizationId = rs.getString("organization_id");
        String communityId = rs.getString("community_id");

        Invite invite;
        switch (type) {
            case COMMUNITY_BY_ORG:
                if (asAdmin) {
                    log.error("unexpected use of invites for assigning an organization to be an admin of a community");
                    return null;
                }
                invite = new CommunityMemberExistingOrgInvite(token, communityId, organizationId);
                break;
            case COMMUNITY_BY_EMAIL:
                if (asAdmin) {
                    invite = new CommunityAdminByEmailInvite(token, communityId, email);
                } else {
                    invite = new CommunityMemberByEmailInvite(token, communityId, email);
                }
                break;
            case USER:
                invite = new OrganizationMemberInvite(token, organizationId, email, asAdmin);
                break;
            case ORGANIZATION_BY_COMMUNITY:
                invite = new OrganizationByCommunityInvite(token, communityId, organizationId, email);
                break;
            default:
                log.error("Unknown invite type " + type.toString());
                return null;
        }

        return invite.setStatus(status);
    }

    private String generateToken() {
        return UUID.randomUUID().toString().replace("-", "");
    }


    // TODO: update
    private Invite createInDb(Invite invite) {
        String sql = "insert into " + NAME + " (token, type, community_id, email, organization_id, as_admin) values ("
                + "'" + invite.token + "', "
                + "'" + invite.type.toString() + "', "
                + invite.getCommunityIdString() + ", "
                + "'" + invite.getEmail() + "', "
                + invite.getOrganizationIdString() + ", "
                + Boolean.toString(invite.asAdmin())
                + ");";
        try {
            execute(sql);
        } catch (DataException e) {
            log.error("Could not create an invite (" + invite.getCommunityId()  + ") invite in the database. " + e.getCause().toString());
            e.printStackTrace();
            return null;
        }

        return invite;
    }


}