package org.ce.assessment.platform.dashboard;

import org.ce.assessment.platform.AssessmentSection;

public class Dashboard {

    public void updateAssessmentSection(AssessmentSection assessmentSection) {
        new Scores().updateAssessmentSection(assessmentSection);
        new Opportunities().updateAssessmentSection(assessmentSection);
        new Challenges().updateAssessmentSection(assessmentSection);
    }


}
