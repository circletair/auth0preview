package org.ce.assessment.limeapi;

public class ParticipantData {

    private String email = "";
    private String firstName = "";
    private String lastName = "";

    public ParticipantData setEmail(String email) {
        this.email = email;
        return this;
    }

    public ParticipantData setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public ParticipantData setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String toJson() {
        return "{ " +
                "\"email\" : \"" + email + "\", " +
                "\"firstname\" : \"" + firstName + "\", " +
                "\"lastname\" : \"" + lastName + "\" }";
    }
}
