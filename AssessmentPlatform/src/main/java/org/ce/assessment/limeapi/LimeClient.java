package org.ce.assessment.limeapi;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.ce.assessment.platform.Platform;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.IOException;

public class LimeClient {
	
	private final static Logger log = LoggerFactory.getLogger(LimeClient.class);

	public final static String LIME_URL = getLimeURL(false);
	public final static String LIME_URL_SSL = getLimeURL(true);

	private static String getLimeURL(boolean ssl) {
		try {
			InitialContext cxt = new InitialContext();
			String limeUrl = Platform.instance().configuration.limeUrl;
			        //(String) ((Context) cxt.lookup("java:/comp/env")).lookup("org.ce.assessment.limeUrl");
			return (ssl)?"https://"+limeUrl:"http://"+limeUrl;
		} catch (NamingException e) {
			log.error("Could not find limeUrl context parameter (should be specified in META-INF/context.xml");
			return "";
		}
	}
	
	private static final String RC_URL = LIME_URL + "/index.php/admin/remotecontrol";
	private HttpClient client = HttpClientBuilder.create().build();
	private final HttpPost post;
	private String sessionKey = null;
	
	private final static LimeClient instance = new LimeClient();
	static LimeClient instance() {
		return instance;
	}
	
	private LimeClient() {
		post = new HttpPost(RC_URL);
		post.setHeader("Content-type", "application/json");
	}
	
	String getSessionKey() {
		if (sessionKey == null) {
			try {
				sessionKey = new GetSessionKey().getKey();
			} catch (LimeCallException e) {
				log.error("Could not get session key. " + e.getErrorMsg());
				e.printStackTrace();
			}
		}		
		return sessionKey;
	}
	
	void invalidateSessionKey() {
		sessionKey = null;
	}

	/** executes the call and returns the result as string */
	String execute(String apiCall) throws ClientProtocolException, IOException {
		post.setEntity(new StringEntity(apiCall));		
		HttpResponse response = client.execute(post);
        if (response.getStatusLine().getStatusCode() == 200){
    		 HttpEntity entity = response.getEntity();
             return EntityUtils.toString(entity);           
        } 
        return null;
	}

}
