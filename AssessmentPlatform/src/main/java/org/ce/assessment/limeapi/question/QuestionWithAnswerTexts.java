package org.ce.assessment.limeapi.question;

import org.ce.assessment.platform.DbQuery;
import org.slf4j.Logger;

import javax.naming.NamingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public abstract class QuestionWithAnswerTexts extends TextQuestion {

    protected final Map<String, String> answerTexts = new HashMap<>();

    protected QuestionWithAnswerTexts(String surveyId, String questionCode) {
        super(surveyId, questionCode);
    }

    protected abstract Logger logger();

    protected void loadAnswerTexts(String limeDataSourceName) {
        String querySQL =
                "SELECT A.* " +
                        "FROM lime_questions L " +
                        "   INNER JOIN lime_answers A ON A.qid = L.qid " +
                        "WHERE " +
                        "  L.sid = " + surveyId + " AND " +
                        "  L.title = '"+ questionCode + "';";
        try (DbQuery selectQuery = new DbQuery(limeDataSourceName, querySQL);
             ResultSet results = selectQuery.getResults())
        {
            while (results.next()) {
                answerTexts.put(results.getString("code"), results.getString("answer"));
            }
        } catch (SQLException | NamingException e) {
            logger().error("Could not get answerTexts texts for question '" + questionCode + "'. SurveyId: " + surveyId);
            logger().error(e.toString());
        }
    }
}
