package org.ce.assessment.limeapi.question;

import org.ce.assessment.limeapi.AbstractQuestionBuilder;
import org.ce.assessment.limeapi.answeroptions.CheckedAnswerOption;
import org.ce.assessment.limeapi.answeroptions.MultipleChoiceAnswerOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MultipleChoiceQuestion extends QuestionWithOptions<MultipleChoiceAnswerOption> {

    private Logger log = LoggerFactory.getLogger(MultipleChoiceQuestion.class);

    public static class Builder extends AbstractQuestionBuilder {
        private final String limeDsName;
        public Builder(String questionCode, String limeDsName) {
            super(questionCode);
            this.limeDsName = limeDsName;
        }

        @Override
        public TextQuestion build(String surveyId) {
            return new MultipleChoiceQuestion(surveyId, questionCode, limeDsName);
        }
    }

    protected MultipleChoiceQuestion(String surveyId, String questionCode, String limeDataSourceName) {
        super(surveyId, questionCode);
        loadOptions(limeDataSourceName);
    }

    @Override
    protected Logger logger() {
        return log;
    }

    @Override
    protected void addOption(String code, String text) {
        MultipleChoiceAnswerOption option = new MultipleChoiceAnswerOption(questionCode, code, text);
        options.put(code, option);
    }

    public List<CheckedAnswerOption> getSelectedOptions() {
        List<CheckedAnswerOption> res = new ArrayList<>();
        for (CheckedAnswerOption option : options.values()) {
            if (option.isSelected()) {
                res.add(option);
            }
        }
        return res;
    }

    public List<CheckedAnswerOption> getUnselectedOptions() {
        List<CheckedAnswerOption> res = new ArrayList<>();
        for (CheckedAnswerOption option : options.values()) {
            if (!option.isSelected()) {
                res.add(option);
            }
        }
        return res;
    }

    public Map<String, MultipleChoiceAnswerOption> getOptions() {
        return options;
    }
}
