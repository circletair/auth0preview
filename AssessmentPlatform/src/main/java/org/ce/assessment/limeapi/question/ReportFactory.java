package org.ce.assessment.limeapi.question;

import org.ce.assessment.limeapi.Survey;
import org.ce.assessment.limeapi.answeroptions.RadioAnswerOption;
import org.ce.assessment.platform.Platform;
import org.ce.assessment.platform.report.ScaleSector;
import org.ce.assessment.platform.report.Sector;
import org.ce.assessment.platform.report.Strategy;

import java.util.function.Function;

public class ReportFactory {

    private final Survey survey;

    public ReportFactory(Survey survey) {
        this.survey = survey;
    }


    private final static Function<RadioAnswerOption, Boolean> IS_APPLICABLE_FUNCTION = getIsApplicableFunction();
    private static Function<RadioAnswerOption, Boolean> getIsApplicableFunction() {
        return new Function<RadioAnswerOption, Boolean>() {
            @Override
            public Boolean apply(RadioAnswerOption option) {
                return !"A1".equals(option.getSelectedValueCode());
            }
        };
    }


    // Questions

    public TextQuestion q(String questionCode) {
        return (TextQuestion) survey.question(new TextQuestion.Builder(questionCode));
    }

    public DateQuestion date(String questionCode) {
        return (DateQuestion) survey.question(new DateQuestion.Builder(questionCode));
    }

    public MultipleChoiceQuestion mcqr(String questionCode) {
        return (MultipleChoiceQuestion) survey.question(
                new MultipleChoiceQuestionWithRelevance.Builder(questionCode, Platform.LIME_DATA_SOURCE_NAME));
    }

    public MultipleChoiceQuestion mcq(String questionCode) {
        return (MultipleChoiceQuestion) survey.question(
                new MultipleChoiceQuestion.Builder(questionCode, Platform.LIME_DATA_SOURCE_NAME));
    }

    public ConditionQuestion mcqOption(String questionCode, boolean isChecked) {
        return new ConditionQuestion(survey, questionCode, "Y", isChecked);
    }

    public ConditionQuestion yesNoOption(String questionCode, boolean isYes) {
        return new ConditionQuestion(survey, questionCode, (isYes)?"A1":"A2", true);
    }

    public ArrayQuestion arrayQuestion(String questionCode) {
        return (ArrayQuestion) survey.question(
                new ArrayQuestion.Builder(questionCode, Platform.LIME_DATA_SOURCE_NAME));
    }

    public ListRadioQuestion radio(String questionCode) {
        return (ListRadioQuestion) survey.question(
                new ListRadioQuestion.Builder(questionCode, Platform.LIME_DATA_SOURCE_NAME));
    }


    // Sectors

    /**
     * Assumes scale sectors are composed of two questions - an array with radio and a text, named
     *  [name][order number] and [name][order number]Ex. e.g:  DesignWaste1 and DesignWaste1Ex
     *
     * @param name to be displayed in the report
     * @param questionCode without the order number
     * @param n - order number of the question
     * @return
     */
    public Sector scaleSector(String name, String questionCode, int n) {
        return new ScaleSector(name, arrayQuestion(questionCode + n)
        )
                .withExtraCondition(IS_APPLICABLE_FUNCTION)
                .withComment(q(questionCode + n + "Ex"));
    }

    public Sector strategy(String name, String questionCode, int n) {
        return new Strategy(name, arrayQuestion(questionCode + n))
                .withExtraCondition(IS_APPLICABLE_FUNCTION)
                .withComment(q(questionCode+n+"Ex"));
    }
}
