package org.ce.assessment.limeapi.question;

import org.ce.assessment.platform.DbQuery;
import org.ce.assessment.platform.Platform;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.NamingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public abstract class Question {

    private Logger log = LoggerFactory.getLogger(Question.class);

    protected final String surveyId;
    public final String questionCode;
    private String text;

    Question(String surveyId, String questionCode) {
        this.surveyId = surveyId;
        this.questionCode = questionCode;
    }

    protected void loadText(String limeDataSourceName) {
        String querySQL =
                "SELECT question as text " +
                        "FROM lime_questions " +
                        "WHERE " +
                        "  sid = " + surveyId + " AND " +
                        "  title = '"+ questionCode + "';";

        try (
                DbQuery selectQuery = new DbQuery(limeDataSourceName, querySQL)
        )
        {
            ResultSet results = selectQuery.getResults();
            if (results.next()) {
                text = results.getString("text");
            }
        } catch (SQLException | NamingException e) {
            log.error("Could not load text for question '" + questionCode + "'. SurveyId: " + surveyId);
            log.error(e.toString());
        }
    }

    public String getText() {
        if (text == null) {
            loadText(Platform.LIME_DATA_SOURCE_NAME);
        }
        return text;
    }

    protected Question setText(String text) {
        this.text = text;
        return this;
    }

    public abstract void setAnswer(Map<String, String> responses);

    public void addQuestionCodesToList(List<String> questionCodes) {
        questionCodes.add(questionCode);
    }

    public String value() {
        throw new UnsupportedOperationException();
    }
}
