package org.ce.assessment.limeapi;

public class LimeCallException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public final LimeCallError error;
	
	public LimeCallException() {
		error = null;
	}
	
	public LimeCallException(LimeCallError error) {
		super(error.msg);
		this.error = error;
	}
	
	public LimeCallException(Exception e) {
		super(e);
		error = null;
	}

	public LimeCallException(String msg) {
		super(msg);
		error = null;
	}
		
	public String getErrorMsg() {
		return (error != null) ? error.msg : "";
	}	
}
