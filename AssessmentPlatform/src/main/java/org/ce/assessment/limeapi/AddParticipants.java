package org.ce.assessment.limeapi;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AddParticipants extends LimeCall {

    private static final Logger log = LoggerFactory.getLogger(AddParticipants.class);

    final private CallParameterArrayObject participantDataParam = new CallParameterArrayObject("aParticipantData");
    private ParticipantData participantData = new ParticipantData();

    @Override
    String methodName() {
        return "add_participants";
    }

    public AddParticipants(Survey survey) {
        super(survey);
        params.add(new CallParameterString("iSurveyID", survey.getId()));
        params.add(participantDataParam);
    }

    public AddParticipants withParticipantData(ParticipantData data) {
        this.participantData = data;
        return this;
    }


    /***
     *
     * @return new token or null if adding new participant failed
     */
    public String addParticipant() {
        participantDataParam.withValue(participantData.toJson());
        try {
            String result = getResult();
            JsonArray jsonArray = parseAsArray(result);
            JsonObject r = jsonArray.get(0).getAsJsonObject();
            if (r.get("errors") != null) {
                log.error("Could not add participant. " + r.get("errors").getAsJsonObject().toString());
                return null;
            }
            String token = r.get("token").getAsString();
            return token;
        } catch (LimeCallException e) {
            log.error("Could not add participant " + e.toString());
            e.printStackTrace();
        }
        return null;
    }

    /***
     *
     * @return new token or null if adding new participant failed
     */
    public String addParticipant(ParticipantData participantData) {
        withParticipantData(participantData);
        return addParticipant();
    }
}
