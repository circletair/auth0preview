package org.ce.assessment.limeapi;

public class CallParameterString extends CallParameter {
	final String value;
	
	public CallParameterString(String name, String value) {
		super(name);
		this.value = value;
	}
	
	@Override
	public String toString() {
		return "\"" + name + "\": \"" + value + "\"";
	}
}

