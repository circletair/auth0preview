package org.ce.assessment.limeapi;

import org.ce.assessment.limeapi.question.Question;

public abstract class AbstractQuestionBuilder {

    protected final String questionCode;

    protected AbstractQuestionBuilder(String questionCode) {
        this.questionCode = questionCode;
    }

    // only to be accessed from Survey
    public abstract Question build(String surveyId);

}
