package org.ce.assessment.limeapi;

import org.ce.assessment.limeapi.question.Question;
import org.ce.assessment.limeapi.question.ReportFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Survey {

    private final static Logger log = LoggerFactory.getLogger(Survey.class);
	
	private final String id;
	private final Map<String, Question> questions = new HashMap<>();
	public final ReportFactory factory = new ReportFactory(this);
	
	
	public Survey(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	
	Iterable<String> getQuestionCodes() {
	    List<String> questionCodes =new ArrayList<>();
	    for (Question q : questions.values()) {
	        q.addQuestionCodesToList(questionCodes);
        }
        return questionCodes;
	}	

	public Question getQuestion(String questionCode) {
		return questions.get(questionCode);
	}

	public Question question(AbstractQuestionBuilder builder) {
	    if (!questions.containsKey(builder.questionCode)) {
	        questions.put(builder.questionCode, builder.build(id));
        }
        return questions.get(builder.questionCode);
    }

	public void loadAnswers(Map<String, String> responses) {
		for (Question q : questions.values()) {
			q.setAnswer(responses);
		}
	}

	public void loadForToken(String token) throws LimeCallException{
        Map<String, String> responses = new ExportResponsesByToken(this, token).getResponses();
        if (responses != null) {
            loadAnswers(responses);
        }
	}

	public static final Survey DUMMY_SURVEY = new Survey("");
}
