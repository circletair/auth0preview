package org.ce.assessment.limeapi.question;

import org.ce.assessment.limeapi.AbstractQuestionBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class ListRadioQuestion extends QuestionWithAnswerTexts {

    private Logger log = LoggerFactory.getLogger(ListRadioQuestion.class);

    public static class Builder extends AbstractQuestionBuilder {
        private final String limeDsName;
        public Builder(String questionCode, String limeDsName) {
            super(questionCode);
            this.limeDsName = limeDsName;
        }

        @Override
        public Question build(String surveyId) {
            return new ListRadioQuestion(surveyId, questionCode, limeDsName);
        }
    }

    @Override
    protected Logger logger() {
        return null;
    }

    private ListRadioQuestion(String surveyId, String questionCode, String limeDataSourceName) {
        super(surveyId, questionCode);
        loadAnswerTexts(limeDataSourceName);
    }

    @Override
    public void setAnswer(Map<String, String> responses) {
        answer = responses.get(questionCode);
    }

    @Override
    public String value() {
        if (answer.isEmpty()) {
            return "";
        }
        return answerTexts.get(answer);
    }
}
