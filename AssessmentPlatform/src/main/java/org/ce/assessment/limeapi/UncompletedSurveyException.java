package org.ce.assessment.limeapi;

public class UncompletedSurveyException extends LimeCallException {
	
	public UncompletedSurveyException() {
		super();
	}
	
	public UncompletedSurveyException(LimeCallError error) {
		super(error);
	}

	private static final long serialVersionUID = -1173672634823700532L;
	
}
