package org.ce.assessment.limeapi.question;

import org.ce.assessment.limeapi.AbstractQuestionBuilder;
import org.ce.assessment.limeapi.answeroptions.RadioAnswerOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class ArrayQuestion extends QuestionWithOptions<RadioAnswerOption> {

    private Logger log = LoggerFactory.getLogger(ArrayQuestion.class);

    public static class Builder extends AbstractQuestionBuilder {
        private final String limeDsName;
        public Builder(String questionCode, String limeDsName) {
            super(questionCode);
            this.limeDsName = limeDsName;
        }

        @Override
        public Question build(String surveyId) {
            return new ArrayQuestion(surveyId, questionCode, limeDsName);
        }
    }

    private ArrayQuestion(String surveyId, String questionCode, String limeDataSourceName) {
        super(surveyId, questionCode);
        loadOptions(limeDataSourceName);
        loadAnswerTexts(limeDataSourceName);
    }

    @Override
    protected Logger logger() {
        return log;
    }

    @Override
    protected void addOption(String code, String text) {
        options.put(code, new RadioAnswerOption(questionCode, code, text, answerTexts));
    }

    public Map<String, RadioAnswerOption> getOptions() {
        return options;
    }
}
