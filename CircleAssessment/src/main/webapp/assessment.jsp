<%@ page import="org.ce.assessment.platform.*" %>
<%@ page import="org.ce.assessment.platform.report.LetterScore" %>
<%
    String assessmentId = request.getParameter("id");
    Assessment assessment = new Assessments().getById(assessmentId);

    if (assessment == null) {
        response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Could not find assessment with id = "
                + assessmentId);
        return;
    }

    AuthManager mgr = new AuthManager();
    User user = mgr.getAuthedUserWithException(request);
%>

<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
    <title>Circle Assessment</title>
    <link href="report.css" rel="stylesheet" type="text/css"/>
</head>

<%@include file="includes.jsp" %>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>

<style>
    img.chart-icon {
        position: absolute;
        height: 50px;
        z-index: 10;
        cursor: pointer;
        left: 50%;
    }

    img.disabled {
        opacity: 0.4;
    }

    img.chart-icon:hover {
        height: 52px;
        margin: -1px 0 0 -1px;
    }


    #opportunities-tab li {
        list-style: disc inside;
    }

    #opportunities-tab h4 {
        color: #323a42;
        font-family: "Montserrat", sans-serif;
        font-size: .7em;
        font-weight: bold;
        text-transform: uppercase;
        margin-top: 3.5em;
    }

    #opportunities-tab h3 {
        color: #292b2d;
        font-family: "Montserrat", sans-serif;
        font-size: 1em;
        font-weight: 400;
        text-transform: none;
        line-height: 300%;
    }



</style>

<script>

    function showSection(sectionId) {
        showReportPage("<%=response.encodeURL("report-t1.jsp")%>", sectionId, "");
    }

    function showT2Report(sectionId, t2Name) {
        showReportPage("<%=response.encodeURL("report-t2.jsp")%>", sectionId, t2Name);
    }

    function showReportPage(url, sectionId, t2Name) {

        $("#clock-div").removeClass("l12");
        $("#clock-div").addClass("l6");


        scrollToSection($("#section-div"));
        $("#section-div").html(
            "<div class=\"progress\">\n" +
            "      <div class=\"indeterminate\"></div>\n" +
            "  </div>"
        );

        $.get(url,
            {
                sectionId: sectionId,
                t2Name: t2Name
            },
            function(data) {
                $("#section-div").html(data);
            }
        ).fail(function (x, y) {
            $("#section-div").html(":(");
            window.alert("Could not load report section");
            window.console.log(x);
        });
    }

    function setExcludedFromDashboard(setExcluded) {
        showLoader();
        $.post(
            "api/assessments/<%=assessment.getId()%>/setExcludedFromDashboard",
            {
                setExcluded: setExcluded
            },
            function() {
                loadDashboard();
            }
        ).fail(function (e) {
            alert("could not set excluded");
            console.log(e.message);
            loadDashboard();
        });
    }
</script>


<body>

    <%@include file="header.jsp" %>

    <div class="container breadcrumbs">
        <%
            if (user.isAdmin()) {
        %>
        <a href="<%=response.encodeURL("communities.jsp")%>">
            Home >
        </a>
        <%
            String communityId = (String) session.getAttribute("communityId");
            if (communityId != null) {
                Community parentCommunity = new Communities().getById(communityId);
        %>
        <a href="<%=response.encodeURL("community.jsp?id="+communityId)%>">
            <%=parentCommunity.getName()%> >
        </a>
        <%  }
        %>
        <a href="<%=response.encodeURL("organization.jsp?id="+assessment.getOrganizationId()+"&" +
         "show=assessments")%>">
            <%=assessment.getOrganization().getName()%> >
        </a>
        <%
        }
        else if (user.isCommunityAdmin()) {
        %>
        <a href="<%=response.encodeURL(mgr.getUserHome(request))%>">
            Home >
        </a>
        <a href="<%=response.encodeURL("organization.jsp?id="+assessment.getOrganizationId()+"&show=assessments")%>">
            <%=assessment.getOrganization().getName()%> >
        </a>
        <% } else { %>
        <a href="<%=response.encodeURL("organization.jsp?id="+assessment.getOrganizationId()+"&show=assessments")%>">
            Home >
        </a>

        <% } %>
    </div>

    <h2>
        <%=assessment.getName()%>
    </h2>

    <ul class="tabs">
        <li class="tab col s3"><a class="active" href="#assessment-tab">Assessment</a></li>
        <li class="tab col s3"><a href="#opportunities-tab">Opportunities</a></li>
        <li class="tab col s3"><a href="#dashboard-tab">Dashboard</a></li>
        <li class="tab col s3"><a href="#print-tab">Print/Pdf</a></li>
    </ul>


    <div id="assessment-tab" class="row body">
        <div class="col l6 m12 s12" id="clock-div">
            <div class="total" id="total" >

                <script src="charts.js"></script>

                <div id="scoreChartContainer" style="position: relative">
                    <%
                        for (AssessmentSection section : assessment.getSections()) {
                            String sectionName = section.getName().replace(" ", "_");
                    %>
                    <img src="images/icons/<%=sectionName%>.png" class="chart-icon" id="icon_<%=sectionName%>"
                         onclick="showSection(<%=section.getId()%>)">
                    <%
                        }
                    %>

                    <%--// TODO move into survey def--%>
                    <style>
                        #icon_Collaborate_to_Create_Joint_Value {margin-left: 85px; top: 77px}
                        #icon_Design_for_the_Future {margin-left: 189px; top: 242px;}
                        #icon_Rethink_the_Business_Model {margin-left: 133px; top: 421px;}
                        #icon_Incorporate_Digital_Technology { margin-left: -72px; top: 477px }
                        #icon_Use_Waste_as_a_Resource {margin-left: -241px; top: 392px }
                        #icon_Prioritise_Regenerative_Resources {margin-left: -284px; top: 211px;}
                        #icon_Preserve_and_Extend_What_Is_Already_Made {margin-left: -137px; top: 68px;}
                    </style>

                    <div id="scoreChart" style="
                                                max-width: 600px;
                                                height: 600px;
                                                margin: 0 auto;
                                                pointer-events: none">
                    </div>
                </div>


                <script>

                <% for (AssessmentSection section: assessment.getSections()) {
                    String iconName = "icon_" + section.getName().replace(" ", "_");
                    int score = section.getScore();
                    if (score == -1) {
                %>
                    $('#<%=iconName%>').addClass('disabled');
                <%
                    }
                }
            %>

                    const reportSections = [
                        <%
                        String comma = "";
                        for (AssessmentSection section : assessment.getSections()) {
                            String sectionDiv= section.getId();
                        %>
                        <%=comma%>"<%=sectionDiv%>"
                        <%
                        comma = ",";
                    }
                    %>
                    ];

                    const data = [
                        <%
                        comma = "";
                        for (AssessmentSection section : assessment.getSections()) {
                            int score = section.getScore();
                            if (score == LetterScore.NOT_APPLICABLE) {
                        %>
                            <%=comma%>{y: 101, name:'<%=section.getName()%>', color:'#fff'}
                        <%
                            } else {
                        %>
                            <%=comma%>{y: <%=score%>, name:'<%=section.getName()%>', color: getColorForStyle('<%=LetterScore.getForScore(score).cssClass%>')}
                        <%
                            }
                        comma = ",";
                        }
                        %>
                    ];
                    drawChart("scoreChart", data, reportSections);

                </script>
            </div>
        </div>
        <div class="col l6 s12 m12">
            <div class="report-page" id="section-div" style="min-height: 700px">
                <div style="display: inline-block; max-width: 500px; margin: 0 auto">
                <h3 style="font-weight: bold; color: #263238">Welcome to the <%=assessment.getName()%>!</h3>

                <p>This assessment will help you to create a baseline overview of all the circular practises
                    and efforts your organisation is currently undertaking. It will enable you to increase
                    your understanding of different aspects of circularity and get inspired by inspirational
                    case studies that illustrate the various circular strategies that you could engage in.</p>

                <p>The assessment is based on a
                    <a href="https://www.circle-economy.com/the-7-key-elements-of-the-circular-economy" target="_blank">
                        comprehensive framework
                    </a>
                    created by Circle Economy. The framework consists of 7 elements that together cover all
                    relevant operational and organisational aspects of the circular economy.</p>

                <p>Each of the 7 sections should take no more than 15-20 minutes to complete. As you complete
                    each section assessment, the visualisation on the left will get updated with your score
                    for that section and summarise your answers. It is possible to leave the survey at any time,
                    just click the <em>‘resume later’</em> button in the top right corner and pick up where you
                    left off another time. If you have already completed a section, you can edit your answer choices
                    at any time. Once you complete all 7 sections, you will get insight into the areas of the
                    circular economy where your company excels, as well as areas where improvements can
                    still be made.</p>


                <h4 style="font-weight: bold">
                    Click on any one of the icons of the 7 elements in the visualisation to the left to get started.
                </h4>

                </div>
            </div>
        </div>
    </div>

    <div id="opportunities-tab">
        <div class="container body">
            <h3>
                Opportunities are applicable circular strategies that you are exploring or have no development yet.
            </h3>
            <%session.setAttribute("assessmentId", assessmentId);%>
            <%@include file="components/opportunities.jsp"%>
        </div>
    </div>

    <div id="dashboard-tab">
        <div class="container body">
            <div style="text-align: center;">
            <%
                session.setAttribute("dashboardCommunityAssessmentId", assessment.getCommunityAssessmentId());
                session.setAttribute("dashboardCommunityName", assessment.getCommunityAssessment().getCommunity().getName());
                session.setAttribute("dashboardCanUpdate", false);
                session.setAttribute("dashboardOwnAssessmentId", assessment.getId());

                if (user.isAdmin() || user.isCommunityAdmin()) {
            %>

                    <input type="checkbox" id="excludedCheckbox"
                           onchange="setExcludedFromDashboard(this.checked)"
                        <%=(assessment.isExcludedFromDashboard()?"checked=\"checked\"":"")%>
                    >
                    <label for="excludedCheckbox">Exclude from dashboard</label>
            <%
                } else if (assessment.isExcludedFromDashboard()) {
            %>
                <span style="color:#ac0025">
                    <i class="tiny material-icons">block</i>
                    This assessment is excluded from the dashboard by the administrator. <br/>
                    You can still view the dashboard and compare your results.
                </span>
            <% } %>
            </div>
            <%@include file="dashboard.jsp"%>
        </div>
    </div>

    <script>
        function doPrint() {
            const level = $("input:radio[name='level-of-detail']:checked").val();
            window.open("print-report.jsp?level="+level+"&id=<%=assessmentId%>");
        }

    </script>
    <div id="print-tab">
        <div class="container body">

            <p>
                <input name="level-of-detail" type="radio" checked id="test1" value="1"/>
                <label for="test1">Chart + Opportunities</label>
            </p>
            <p>
                <input name="level-of-detail" type="radio" id="test2" value="2"/>
                <label for="test2">Chart + Opportunities + Scorecards</label>
            </p>
            <p>
                <input name="level-of-detail" type="radio" id="test3" value="3"/>
                <label for="test3">Full Details</label>
            </p>

            <button onclick="doPrint()" class="btn grey lighten-3 black-text">Print</button>
            <p><em>To get a Pdf click Print and then choose "To Pdf" as your printer</em></p>
        </div>
    </div>


<%@include file="footer.jsp"%>
</body>
</html>
