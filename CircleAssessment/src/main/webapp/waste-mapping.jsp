<%@ page import="org.ce.assessment.fashiontool.WasteMapping" %>
<%@ page import="org.ce.assessment.userdb.Organization" %>
<%@ page import="org.ce.assessment.userdb.Organizations" %>
<%

    String host = request.getServerName();
    host = (("localhost".equals(host))?"http://":"https://") + host;

    if (request.getServerPort() != 80) {
        host += ":" + request.getServerPort();
    }
    host += "/";

    AuthManager mgr = new AuthManager();

    String id = request.getParameter("id");
    Organization organization = new Organizations().getById(id);
    String backUrl = response.encodeURL("organization.jsp?show=waste&id=" + id);

    if (organization == null) {
        response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Could not find organization with id = "
                + id);
        return;
    }

    WasteMapping wasteMapping = WasteMapping.getForOrganization(organization);
    if (wasteMapping == null) {
        return;
    }

    // TODO: enable auth
//    User user = mgr.getAuthedUserWithException(request);
//    if (!user.isAdmin()) {
//        response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
//    }

%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Circle Assessment</title>
</head>
<%@include file="includes.jsp" %>

<script>
    {
       // THIS DOES NOT WORK IN DEV PREVIEW


        var onMessage = function (event) {
            const action = event.data.action;

            if (action === 'complete') {
                location.replace('<%=backUrl%>');
            }
            else if (action === 'resize') {
                $('#surveyFrame').height(event.data.height+100);
            }
            else if (action === 'scrollup') {
                window.scrollTo(0,0);
            }
            else if (action === 'alert') {
                $("#alert-modal-content").html(event.data.message);

                if ("Your answers were successfully saved." === event.data.message) {
                    $("#alert-modal-content").html(
                        event.data.message + " Taking you back to the organization profile."
                        + "<div class=\"progress\">\n" +
                        "      <div class=\"indeterminate\"></div>\n" +
                        "  </div>"
                    );

                    setTimeout(function() {
                        location.replace('<%=backUrl%>');
                    }, 1500);
                }

                $("#alert-modal").modal({complete: function() {$("#alert-modal-content").html("");}});
                $("#alert-modal").modal('open');
            }
            else {
                console.error("unknown action: " + action);
            }
        }

        if (window.addEventListener) {
            window.addEventListener("message", onMessage, false);
        } else if (window.attachEvent) {
            window.attachEvent("onmessage", onMessage);
        }
    }
</script>

<style>
    iframe {
        border: none;
        overflow-y: hidden;
        margin-top: 40px;
        width: 100%;
    }
</style>


<body>
<%@include file="header.jsp" %>

<div class="container">
    <div class="breadcrumbs">
        <a href="<%=backUrl%>">
            < <%=organization.getName()%>
        </a>
    </div>
</div>
<iframe src="<%=wasteMapping.getSurveyLink(false)%>" width="100%" height="100%" id="surveyFrame" scrolling="no"></iframe>

<div id="alert-modal" class="modal">
    <div class="modal-content" id="alert-modal-content">
    </div>
    <div class="modal-footer">
        <button class="btn-flat waves-effect waves-light btn-card cancel modal-close">OK</button>
    </div>
</div>

<%@include file="footer.jsp"%>
</body>
</html>
