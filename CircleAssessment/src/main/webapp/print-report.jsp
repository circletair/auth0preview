<%@ page import="org.ce.assessment.platform.Assessment" %>
<%@ page import="org.ce.assessment.platform.AssessmentSection" %>
<%@ page import="org.ce.assessment.platform.Assessments" %>
<%@ page import="org.ce.assessment.platform.AuthManager" %>
<%@ page import="org.ce.assessment.platform.report.LetterScore" %>
<%@ page import="org.ce.assessment.platform.report.Section" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="static org.ce.assessment.platform.report.LetterScore.NOT_APPLICABLE" %>
<%@ page import="java.util.Date" %>
<%
    String assessmentId = request.getParameter("id");
    Assessment assessment = new Assessments().getById(assessmentId);

    if (assessment == null) {
        response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Could not find assessment with id = "
                + assessmentId);
        return;
    }

    AuthManager mgr = new AuthManager();

    // TODO:
//    User user = mgr.getAuthedUserWithException(request);
//    if (!user.isAdmin()) {
//        response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
//    }

    int level = Integer.parseInt(request.getParameter("level"));

%>
<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
    <title>Circle Assessment</title>
    <link href="report.css" rel="stylesheet" type="text/css"/>
    <link href="report-print.css" rel="stylesheet" type="text/css"/>

</head>

<%@include file="includes.jsp" %>

<style>
    img.chart-icon {
        position: absolute;
        height: 50px;
        z-index: 10;
        cursor: pointer;
        left: 50%;
    }

    img.disabled {
        opacity: 0.4;
    }

    @media screen {
        .screen-hidden {
            display: none;
        }
    }

    @media print {
        .screen-hidden {
            display: unset;
        }
    }

</style>

<script>
    $(function() {setTimeout(
        function() {
            window.print();
            $("#msg").html("You can <a href=\"javascript: window.close()\">close</a> this window now");
        }, 10000);});
</script>

<body>
    <div class="no-print container" style="text-align: center; padding: 100px">
        <h3 id="msg">
            <div class="progress">
                <div class="indeterminate"></div>
            </div>
            Preparing for printing
        </h3>
    </div>
    <div class="screen-hidden">
    <h2>
        <%=assessment.getOrganization().getName()%>
    </h2>
    <h1 class="print-header">
        <%=assessment.getName()%>
    </h1>
    <div style="text-align: center">
        <%=(new SimpleDateFormat("dd MMM yyyy").format(new Date()))%>
    </div>

    <div class="total" id="total" >
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/highcharts-more.js"></script>
        <script src="charts.js"></script>

        <div id="scoreChartContainer" style="position: relative">
            <%
                for (AssessmentSection section : assessment.getSections()) {
                    String sectionName = section.getName().replace(" ", "_");
            %>
            <img src="images/icons/<%=sectionName%>.png" class="chart-icon" id="icon_<%=sectionName%>"
                 onclick="showSection(<%=section.getId()%>)">
            <%
                }
            %>

            <%--// TODO move into survey def--%>
            <style>
                #icon_Collaborate_to_Create_Joint_Value {margin-left: 85px; top: 77px}
                #icon_Design_for_the_Future {margin-left: 189px; top: 242px;}
                #icon_Rethink_the_Business_Model {margin-left: 133px; top: 421px;}
                #icon_Incorporate_Digital_Technology { margin-left: -72px; top: 477px }
                #icon_Use_Waste_as_a_Resource {margin-left: -241px; top: 392px }
                #icon_Prioritise_Regenerative_Resources {margin-left: -284px; top: 211px;}
                #icon_Preserve_and_Extend_What_Is_Already_Made {margin-left: -137px; top: 68px;}
            </style>

            <div id="scoreChart" style="
                                                        max-width: 600px;
                                                        height: 600px;
                                                        margin: 0 auto;
                                                        pointer-events: none">
            </div>
        </div>


        <script>

            <% for (AssessmentSection section: assessment.getSections()) {
                String iconName = "icon_" + section.getName().replace(" ", "_");
                Integer score = section.getScore();
                if (score == -1) {
            %>
            $('#<%=iconName%>').addClass('disabled');
            <%
                }
            }
        %>

            var reportSections = [
                <%
                String comma = "";
                for (AssessmentSection section : assessment.getSections()) {
                    String sectionDiv= section.getId();
                %>
                <%=comma%>"<%=sectionDiv%>"
                <%
                comma = ",";
            }
            %>
            ];

            var data = [
                <%
                comma = "";
                for (AssessmentSection section : assessment.getSections()) {
                    int score = section.getScore();
                    if (score == NOT_APPLICABLE) {
                %>
                    <%=comma%>{y: 101, name:'<%=section.getName()%>', color:'#fff'}
                <%
                    } else {
                %>
                    <%=comma%>{y: <%=score%>, name:'<%=section.getName()%>', color: getColorForStyle('<%=LetterScore.getForScore(score).cssClass%>')}
                <%
                    }
                comma = ",";
                }
                %>
            ];
            drawChart("scoreChart", data, reportSections);

        </script>
    </div>
        
    <div id="opportunities">
        <h3>Key Opportunities</h3>
        <% session.setAttribute("assessmentId", assessmentId);%>
        <%@include file="components/opportunities.jsp"%>
    </div>



    <%
        for (AssessmentSection section : assessment.getSections()) {
            String url = "report-t1.jsp?sectionId="+section.getId();
            if (level >= 2) {
    %>
        <jsp:include page="<%=url%>" />
    <%
            }
            if (level == 3 && section.getReport().isSurveyCompleted()) {
                for (Section t2Section : section.getReport().sections.values()) {
                    if (t2Section.getScore() != NOT_APPLICABLE) {
                        String t2url = "report-t2.jsp?sectionId="+section.getId() + "&t2Name="+t2Section.getName();
    %>
        <jsp:include page="<%=t2url%>" />
    <%
                    }
                }
            }
        }

    %>


    </div>
</body>
</html>
