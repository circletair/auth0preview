<%@page import="org.ce.assessment.userdb.Organization"%>
<%@ page import="org.ce.assessment.userdb.Organizations" %>
<%@ page import="java.util.List" %>

<%
    AuthManager mgr = new AuthManager();
    List<Organization> organizations;

    String select = request.getParameter("select");
    if ("all".equals(select)) {
         organizations = new Organizations().getAll();
    } else if ("unattached".equals(select)) {
        organizations = new Organizations().getUnattached();
    } else {
        response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid select parameter: " + select + ". Values values: 'all', 'unattached");
        return;
    }

    mgr.confirmAuthAsAdmin(request);
    session.setAttribute("communityId", null);
%>

<%@ page contentType="text/html; charset=ISO-8859-1"
         pageEncoding="UTF-8"%>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="main-style.css" rel="stylesheet" type="text/css">
	<title>Circle Assessment</title>
</head>

<%@include file="includes.jsp" %>


<style>
	a.link-to-assessment, a.link-to-assessment:VISITED  {
		display: inline-block;
		text-decoration: none;
		color: #263238;
	}
	
	a.link-to-assessment:HOVER {
		color: #00c1f2;
        background-color: #fafafa;
	}

    a#offline-btn {
        color: white;
        float: right;
    }

    span.client-name {
        display: inline-block;
        width: 30em;
        padding: 10px 30px;
    }
    span.client-score {
        display: inline-block;
        width: 10em;
        padding: 10px;
    }

    div.company_box {
        width: 700px;
        text-align: left;
        position: relative;
        left:50%;
        margin-left: -350px;
        margin-top:15px;
    }

    table.company_list {
        width: 100%;
        background-color: white;
        text-align: left;
        margin: 0;
        min-height: 25em;
    }

    table.company_list tr:hover {
        background-color: rgba(200,200,200,0.1);
        font-weight: bold;
    }

    table.company_list .header {
        box-shadow: 0 3px 15px #aaa;
        font-weight: bold;
        margin-bottom: 10px;
    }

    table.company_list td.score-cell {
        width: 55px;
        min-width: 30px;
        text-align: center;
        font-weight: bold;
        font-family: sans-serif;
        border: 1px solid white;
        opacity: 0.7;
        color:white;
    }

        table.company_list tr:hover td.score-cell {
            opacity:1;
        }





    table.company_list th img {
        width: 40px;
    }

    table.company_list td.company-name, table.company_list th {
        padding: 1px;
    }

    i.sort {
        font-size: 0.8rem;
        color: #ddd;
    }
    i.sort-blue {
        font-size: 0.8rem;
        color: #777;
    }

    tr.client-line {
        height: 2em;
    }




</style>


<body class="landing">
    <%@include file="header.jsp" %>

    <div class="container white" style="padding: 50px 30px">

        <h2><%=("all".equals(select))?"All":"Unattached"%> organizations</h2>

        <ul>
            <%
                for (Organization organization : organizations) {
            %>
            <li><a href="<%=response.encodeURL("organization.jsp?id=" + organization.id)%>"><%=organization.getName()%></a></li>
            <%
                }
            %>
        </ul>

        <br><br><br>
        <%@include file="footer.jsp"%>
    </div>
</body>
</html>
