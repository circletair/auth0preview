<%@ page import="org.ce.assessment.platform.AssessmentVersion" %>
<%@ page import="org.ce.assessment.platform.AssessmentVersionSection" %>
<%@ page import="org.ce.assessment.platform.Platform" %>
<%
    AuthManager mgr = new AuthManager();

    String assessmentVersionId = request.getParameter("id");
    AssessmentVersion assessmentVersion = Platform.instance().getAssessmentVersions().getById(assessmentVersionId);

    if (assessmentVersion == null) {
        response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Could not find assessment version with id = "
                + assessmentVersionId);
        return;
    }


//    User user = mgr.getAuthedUserWithException(request);
//    if (!user.isAdmin()) {
//        response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
//    }

%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Circle Assessment</title>
</head>

<%@include file="includes.jsp" %>

<script>

    const url = '<%=response.encodeURL("api/assessment-versions/" + assessmentVersionId + "/sections/")%>';

    function deleteAssessmentVersion() {
        $.ajax({
            method: 'post',
            url: '<%=response.encodeURL("api/assessment-versions/"+assessmentVersionId + "/delete/")%>',
            data: {}
        })
            .done(
                function() {
                    location.reload(true);
                }
            )
            .fail(
                function (jqXHR, error, details) {
                    alert(error);
                }
            );
    }

    function activate() {
        $.ajax({
            method: 'post',
            url: '<%=response.encodeURL("api/assessment-versions/"+assessmentVersionId)+"/activate/"%>',
            data: {
                activate: true
            }
        })
            .done(
                function() {
                    location.reload();
                }
            )
            .fail(
                function (jqXHR, error) {
                    alert(error);
                }
            );
    }


    function deleteSection(id) {
        const url = 'api/assessment-versions/' + <%=assessmentVersionId%> + '/sections/' + id + '/delete/';
        $.ajax({
            method: 'post',
            url: url,
            data: {
                id: id
            }
        })
            .done(function (jqXHR, status, details) {
                location.reload();
            })
            .fail(function (jqXHR, error) { alert(error); })
    }

    function createSection() {
        const kind = $("#in-kind").val();
        const name = $("#in-name").val();
        const surveyId = $("#in-surveyid").val();

        $.ajax({
            method: 'post',
            url: url,
            data:
                {
                    kind: kind,
                    name: name,
                    surveyId: surveyId
                }
        })
        .done(
            function (jqXHR, status, details) {
                location.reload();
            })
        .fail(
            function (jqXHR, error) { alert(error);  }
        );
    }

</script>

<body>
    <%@include file="header.jsp" %>
    <div class="container body white">
        <h2>
            <a href="communities.jsp#assessments-tab"><i class="material-icons">arrow_back</i> Assessment Versions: </a>
            <%=assessmentVersion.getName()%>
        </h2>
<% if (false) { %>
        <button class="btn white grey-text" onclick="deleteAssessmentVersion()">Delete</button>
        <button class="btn white grey-text" onclick="activate()">Activate</button>
<% } %>

        <table class="highlight">
            <thead><tr>
                <th>Kind</th>
                <th>Section</th>
                <th>Survey Id</th>
                <th></th>
            </tr></thead>

        <% for (AssessmentVersionSection section : assessmentVersion.getSectionList()) { %>
            <tr>
                <td><%=section.getKind()%></td>
                <td><%=section.getName()%></td>
                <td><%=section.getLimeSurveyId()%></td>
                <td><button class="btn-flat white grey-text" onclick="deleteSection(<%=section.getId()%>)">Delete</button></td>
            </tr>
        <% } %>

            <tr>
                <td>
                    <input type="text" id="in-kind">
                </td>
                <td>
                    <input type="text" id="in-name">
                </td>
                <td>
                    <input type="text" id="in-surveyid">
                </td>
                <td>
                    <button onclick="createSection()" class="btn-flat white grey-text">new</button>
                </td>
            </tr>
        </table>



    </div>
    <%@include file="footer.jsp" %>
</body>
</html>
