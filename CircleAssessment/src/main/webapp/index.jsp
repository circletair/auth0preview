
<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%
    AuthManager mgr = new AuthManager();
    User authedUser = mgr.getAuthedUser(request);
    if (authedUser != null && !authedUser.isUnKnown()) {
        request.getRequestDispatcher(response.encodeURL(mgr.getUserHome(request))).forward(request, response);
        return;
    }
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Circle Assessment</title>
    <link rel="icon" type="image/png" href="images/favicon.png">
</head>

<%@include file="includes.jsp" %>


<style>

    /* Fix for auto fill */
    input:-webkit-autofill + label {
        -webkit-transform: translateY(-14px) scale(0.8);
        transform: translateY(-14px) scale(0.8);
        -webkit-transform-origin: 0 0;
        transform-origin: 0 0;
    }

    .login-form {
        display: inline-block;
        position: relative;
        right: 0;
        margin: 30px -10px;
        max-width: 400px;
        padding: 20px 40px 40px 40px;
        border: 1px solid #ddd;
        background-color: #fff;
    }
        .login-form a {
            color: #263232;
            font-size: .8em
        }

        .login-form a#reset_pw_link {
            margin-right:30px;
        }

    ul {
        padding-left: 2em !important;
        margin: 0 0 1em 0 !important;
    }
    li {
        list-style-type: disc !important;
    }

</style>

<script>
    function resetModal() {
        $("#reset-form").removeClass("hidden");
        $("#reset-sending").addClass("hidden");
        $("#reset-success").addClass("hidden");
        $("#reset-failed").addClass("hidden");
        $('#reset-email').removeClass("invalid");

        $("#request-form").removeClass("hidden");
        $("#request-success").addClass("hidden");
        $("#request-failed").addClass("hidden");
    }

    function sendReset() {
        $("#reset-form").addClass("hidden");
        $("#reset-sending").removeClass("hidden");

        $.post('<%=response.encodeURL("api/auth/reqest-password-reset/")%>',
            {
                email: $('#reset-email').val()
            },
            function() {
                $("#reset-sending").addClass("hidden");
                $("#reset-success").removeClass("hidden");
            }
        )
        .fail(
            function(reason) {
                if (reason.status === 400) {
                    $('#reset-email').addClass("invalid");
                } else if (reason.status === 500) {
                    $("#reset-sending").addClass("hidden");
                    $("#reset-failed").removeClass("hidden");
                }
            }
        );
    }

    function signIn() {

        $.ajax({
            url: '<%=response.encodeURL("api/login/")%>',
            method: 'get'

        })
            .done(
                function(data) {
                   window.location.replace(data);
                })
            .fail(function(data) {
                if (data.status === 400) {
                    alert(data.responseText);
                } else if (data.status === 401) {
                    alert(data.responseText);
                } else {
                    alert(data.responseText);
                }
            });

        return false;
    }

    $(function() {
        $("#login-form").on("submit", function(e) {
            signIn();
            e.preventDefault();
        });
    });


    $(function() {
        $('.modal').modal();
        Materialize.updateTextFields();
    });
</script>


<body>
<%@include file="header.jsp" %>
    <div class="container row">
        <div class="col m5 s12">
            <form class="login-form" id="login-form" action="#" method="get">
                <button class="btn grey lighten-3 black-text wide" type="submit">Sign in</button>
            </form>
        </div>
        <div class="col m7 s12">
            <img src="images/radar-screen.jpg" style="width: 100%; max-width: 600px; margin: 10px">
            <p>Circle Assessment is an online tool to help businesses understand the different operational and
                organisational aspects of the circular economy.</p>

            By completing the Circle Assessment, your business will be able to understand:
                <ul>
                    <li>The
                        <a href="http://www.circle-economy.com/the-7-key-elements-of-the-circular-economy" target="_blank">
                            key elements of the circular economy
                        </a>
                        that are practical for businesses</li>
                    <li>Strategies to implement circular thinking in your business</li>
                    <li>Inspirational case studies and examples of best practices</li>
                </ul>


            Once you complete the assessment, you will receive a report with a summary of:
            <ul>
                <li>A score of how well your business follows the elements of the circular economy</li>
                <li>The current circular strategies that your business follows</li>
                <li>Future opportunities for circular strategies that your business can pursue</li>
                <li>Key challenges that your business needs to address to implement circularity</li>
            </ul>

            <p>Using the outcomes of the Circle Assessment, your business can start internal and external
                discussions to develop a strategy to incorporate circular economy thinking to extract greater
                value from new business opportunities, become more future-proof, and improve resource use and
                collaboration with the value chain.</p>


            <p style="font-weight: bold;">
                Write us at
                <a href="mailto:hello@circle-lab.com?&subject=Requesting access to Circle Assessment&body=Hi%20there,%0A%0AMy%20name%20is%20__YOUR_NAME__%20and%20I%20would%20like%20to%20request%20access%20to%20Circle%20Assessment%20on%20behalf%20of%20_YOUR_ORGANISATION__.%0A%0AThanks,%0A%0A__YOUR_NAME__">
                hello@circle-lab.com
                </a>
                 to request access.
            </p>
        </div>
    </div>

    <div id="forgot_password_modal" class="modal" style="top: 45%">
        <h5>Forgot your password?</h5>
        <div id="reset-form">
            Fill in your email and we will send you a link to reset it
            <br><br>
            <div class="input-field">
                <input name="<%=AuthManager.fields.EMAIL%>" id="reset-email" placeholder="Email" autocomplete="off">
                <label for="<%=AuthManager.fields.EMAIL%>" data-error="email is not registered" class="active">&nbsp;</label>
            </div>

            <div class="modal-footer">
                <button class="btn-flat waves-effect waves-light btn-card cancel modal-close">Cancel</button>
                <button onclick="sendReset()" class="btn waves-effect waves-light btn-card">Submit</button>
            </div>
        </div>

        <div id="reset-sending" class="hidden">
            <div class="modal-content">
                Sending you an email ... please wait
            </div>
            <div class="modal-footer">
                <button class="btn-flat waves-effect waves-light btn-card modal-close">Cancel</button>
            </div>
        </div>

        <div id="reset-success" class="hidden">
            <div class="modal-content">
                Check your email for the reset link.
            </div>
            <div class="modal-footer">
                <button class="btn waves-effect waves-light btn-card modal-close">OK</button>
            </div>
        </div>
        <div id="reset-failed" class="hidden">
            Internal server error - could not send rest email. Try again later.

            <div class="modal-footer">
                <button class="btn-flat waves-effect waves-light btn-card modal-close">OK</button>
            </div>
        </div>
    </div>

    <%@include file="footer.jsp"%>
</body>
</html>