
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<%--<script src="https://use.fontawesome.com/46cc4115d7.js"></script>--%>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<script>
    $(function(){
        $('.modal').modal();
    });
</script>

<%--Should be after Materialize.css--%>
<link href="main-style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="styles.css">
