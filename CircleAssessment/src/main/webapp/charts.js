
function scrollToSection(section) {
    $("html, body").animate({
        scrollTop: (section.offset().top-250)
    },500);
}


var scoreLetters = {101: "N/A", 90: "A+",  70: "A", 50: "B", 30: "C", 10: "D", 2: "E", 0: "F"};
function getScoreLetter(score) {
    var m = 0;
    for (var k in scoreLetters)  {
        if (score > k) {
            m = k;
        }
    }
    return scoreLetters[m];
}

function getColorForStyle(styleClass) {
    return {
        "score-Aplus" : "#00bf6d",
        "score-A" : "#008040",
        "score-B" : "#dce300",
        "score-C" : "#dd7b00",
        "score-D" : "#df1b00",
        "score-E" : "#ac0025",
        "score-NA" : "#c0c0cc"
    }[styleClass];
}

function getColorForLetter(letterScore) {
    return {
        "A+": "#00bf6d",
        "A" :"#008040",
        "B" : "#dce300",
        "C" : "#dd7b00",
        "D" : "#df1b00",
        "E" : "#ac0025",
        "F" : "#c0c0cc"
    }[letterScore];
}

function drawChart(chartContainer, data, reportSections) {
    Highcharts.chart(chartContainer, {
        chart: {
            polar: true,
            style: {
                zIndex: 20
            }
        },

        exporting: {enabled: false},
        credits: {enabled: false},

        title: {
            text: ""
        },

        pane: {
            startAngle: 5,
            endAngle: 365
        },

        xAxis: {
            tickInterval: 51.40,
            min: 0,
            max: 360,
            labels: {
                formatter: function () {
                    return "";
                }
            }
        },

        yAxis: {
            min: 10,
            max: 150,
            tickPositions: [-1, 10, 30, 50, 70, 90, 100, 150],
            labels: {
                align: "left",
                x: 5,
                y: -2,
                formatter: function () {
                    return scoreLetters[this.value];
                },
                style: {
                    "fontFamily": "sans-serif",
                    "fontWeight": 900
                }
            }
        },

        plotOptions: {
            series: {
                pointStart: 0,
                pointInterval: 51.43
            },
            column: {
                pointPadding: 0,
                groupPadding: 0
            }

        },

        tooltip: {
            formatter: function () {
                if (this.y === 101) {
                    return "<b>" + this.key +
                        "</b><br> not applicable";
                } else {
                    return "<b>" + this.key +
                        "<br>" +
                        "<span style=\"font-size: 16px; " +
                        "font-weight: 900; " +
                        "font-family: sans-serif; " +
                        "color: " + this.color + "\">" +
                        getScoreLetter(this.y) + "</span> " +
                        this.y +
                        "%</b>";
                }
            }
        },

        series: [{
            type: "column",
            name: " ",
            showInLegend: false,
            data: data,
            pointPlacement: "between",
            point: {
                events: {
                    click: function () {
                        showSection(reportSections[this.index]);
                    }
                }
            }
        }]
    });
}

function drawSmallChart(chartContainer, data) {
    Highcharts.chart(chartContainer, {
        chart: {
            polar: true,
            style: {
                zIndex: 20
            },
            margin: [0, 0, 0, 0],
            height: 250
        },

        exporting: {enabled: false},
        credits: {enabled: false},

        title: {
            text: ""
        },

        pane: {
            startAngle: 5,
            endAngle: 365
        },

        xAxis: {
            tickInterval: 51.40,
            min: 0,
            max: 360,
            labels: {
                formatter: function () {
                    return "";
                }
            }
        },

        yAxis: {
            min: 10,
            max: 100,
            tickPositions: [-1, 10, 30, 50, 70, 90, 100],
            labels: {
                formatter: function () {
                    return "";
                }
            }
        },

        plotOptions: {
            series: {
                pointStart: 0,
                pointInterval: 51.43
            },
            column: {
                pointPadding: 0,
                groupPadding: 0
            }

        },

        tooltip: {
            enabled: false
        },

        series: [{
            type: "column",
            name: " ",
            showInLegend: false,
            data: data,
            pointPlacement: "between"
        }]
    });
}
