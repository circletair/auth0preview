<%@page import="org.ce.assessment.limeapi.LimeCallException"%>
<%@page import="org.ce.assessment.platform.ActionNotAllowedExcpetion"%>
<%@page import="org.ce.assessment.platform.ContentNotFoundException"%>
<%@page import="org.ce.assessment.platform.NotAuthorizedException"%>
<%@page import="org.ce.assessment.platform.NotSignedInException"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Error - Circle Assessment</title>
</head>

<style>
    div.centered {
        position:relative;
        left:50%;
        margin-left:-500px; /*this is half of width of your div*/
        text-align: center;
        width: 1000px;
    }

    .page_message {
        border: 1px solid #bbb;
        box-shadow: 3px 3px 15px #ccc;
        width: 80%;
        padding: 1em 0;
        margin: 40px 10%;
        font-size: 18px;
    }
</style>

<% 
	String errMsg = "";
	Throwable t = (Throwable)request.getAttribute("javax.servlet.error.exception");
	if (t == null) {
		errMsg = "Something went wrong :( Fixer-monkeys have been dispatched!<br><span style=\"font-size: 0.7rem\">";
	} else {
        if (t instanceof ServletException && t.getCause() != null) {
            t = t.getCause();
        }
        if (t instanceof NotSignedInException) {
            request.getRequestDispatcher("index.jsp?action=nouser").forward(request, response);
        } else if (t instanceof NotAuthorizedException) {
            errMsg = "You not authorized to see requested content.";
        } else if (t instanceof ActionNotAllowedExcpetion) {
            errMsg = "You are not allowed to " + t.getMessage();
        } else if (t instanceof ContentNotFoundException) {
            errMsg = "Requested content not found.";
        } else {
            errMsg = "Something went wrong :( Fixer-monkeys have been dispatched!<br><span style=\"font-size: 0.7rem\">";
            if (t instanceof LimeCallException) {
                LimeCallException e = (LimeCallException) t;
                errMsg += e.error.toString();
            } else {
                errMsg += t.toString();
            }
            errMsg += "</span>";
        }
    }
%>

<body>
	<div class="centered">
		<div class="page_message">
			<span style="color: red">Error: </span><%=errMsg %>
		</div>
	</div>

</body>
</html>