<script>

    function selectOrganization(id, name) {
        $('#select-org-name').val(name);
        $("#existing-list").empty();
        selectedOrganizationId = id;
    }

    function onType() {
        selectedOrganizationId = -1;
        var name = $('#select-org-name').val();


        $.get(
            'api/organizations',
            {
                query: name
            },
            function(data, status) {
                $("#existing-list").html("");
                var orgs = JSON.parse(data).organizations;

                for(var i in orgs) {
                    var id = orgs[i].id;
                    var name = orgs[i].name;
                    $("#existing-list").append('<li><a href="javascript: selectOrganization(' + id + ', \''+name+'\')">' + name + '</a></li>');
                }
            }
        )
        .fail(
            function(reason) {
                console.log("Could not get list of organizations by name query");
                console.log(reason);
            }
        );
    }
</script>


<input type="text" id="select-org-name" onkeyup="onType()">
<label for="select-org-name">organization name</label>
<ul id="existing-list"></ul>