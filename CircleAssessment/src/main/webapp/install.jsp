<%@ page import="org.ce.assessment.platform.Installer" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%
	Installer installer = new Installer();
	String action = request.getParameter("action");
	String msg = "";
	if ("createtables".equals(action)) {
		msg = (installer.install()) ? "Created tables" : "Failed to create tables";
	}
	else if ("createadmin".equals(action)) {
	    String email = request.getParameter("email");
	    msg = (installer.createAdminUser(email))?"Created admin":"Failed to create admin";
	}
%>    
    
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Circle Assessment Installer</title>
</head>
<body>
	  
	<h1><%=msg %></h1> 
	<a href="index.jsp">Go to the main page.</a>
	<br>
	<a href="install.jsp?action=createtables">Create tables</a>
	<br>
	<form action="install.jsp">
		<input type="hidden" name="action" value="createadmin"/>
		<input name="email">
		<button type="submit">create admin</button>
	</form>
	

</body>
</html>