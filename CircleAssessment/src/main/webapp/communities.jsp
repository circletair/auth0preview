<%@ page import="org.ce.assessment.platform.AssessmentVersion" %>
<%@ page import="org.ce.assessment.platform.Communities" %>
<%@ page import="org.ce.assessment.platform.Community" %>
<%@ page import="org.ce.assessment.platform.Platform" %>
<%@ page import="org.ce.assessment.userdb.Organizations" %>
<%@ page import="java.util.List" %>

<%
    AuthManager mgr = new AuthManager();
    mgr.confirmAuthAsAdmin(request);
    User user = mgr.getAuthedUserWithException(request);
%>

<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="main-style.css" rel="stylesheet" type="text/css">
	<title>Circle Assessment - Communities</title>
</head>

<%@include file="includes.jsp" %>


<script>
   function createCommunity() {
        var name = $("#new-community-name").val();
        if (name !== null) {
            $.ajax({
                    method: 'post',
                    url: '<%=response.encodeURL("api/communities/create/")%>',
                    data: {name: name}
                })
                .done(
                    function (x, data, status) {
                        location.reload();
                    }
                )
                .fail(function(x) {
                    alert(x.responseText);
                });
        }
    }


   /* Unused */
   function createAssessmentVersion() {
       var name = $("#assessment-name").val();

       $.ajax({
           method: 'post',
           url: '<%=response.encodeURL("api/assessment-versions/create/")%>',
           data:
               {
                   name: name
               }
       })
       .done(
           function() {
               location.reload();
           }
        )
       .fail(
           function (jqXHR, error) {
               alert(jqXHR.responseText);
           }
       );
   }


</script>


<body>
    <%@include file="header.jsp" %>

    <h2>Administration Panel</h2>

    <ul class="tabs">
        <li class="tab col s1"><a href="#communities-tab">Communities</a></li>
        <li class="tab col s1"><a href="#assessments-tab">Assessment Versions</a></li>
    </ul>

    <div class="container body">
        <div id="communities-tab" class="col s12">
            <div id="new_community_modal" class="modal">
                <h5>Create community</h5>
                <div class="modal-content">
                    <div class="input-field">
                        <input type="text" name="new-community-name" id="new-community-name">
                        <label for="new-community-name">Community name</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn-flat waves-effect waves-light btn-card cancel modal-close">Cancel</button>
                    <button onclick="createCommunity()" class="btn waves-effect waves-teal btn-card">Create</button>
                </div>
            </div>

            <table class="highlight">
                <thead>
                <tr>
                    <td colspan="2">
                        <button data-target="new_community_modal"
                                class="btn grey lighten-3 black-text modal-trigger">
                            Create
                        </button>
                    </td>
                </tr>
                </thead>

                <%
                    final List<Community> communities = new Communities().getCommunitiesList();
                    for (Community c : communities) {
                %>
                <tr>
                    <td class="nowrap-column">
                        <a href="<%=response.encodeURL("community.jsp?id=" + c.id)%>"><%=c.getName()%></a>
                    </td>
                    <td width="100%">
                        <%=c.getNMembers()%> members
                    </td>
                </tr>
                <%
                    }
                %>
                <tr><td colspan="2">-----</td></tr>
                <tr>
                    <td class="nowrap-column">
                        <a href="<%=response.encodeURL("organizations.jsp?select=all")%>">
                            All organizations
                        </a>
                    </td>
                    <td width="100%">
                        <%=new Organizations().getCount()%>
                    </td>
                </tr>
            </table>
        </div>

        <div id="assessments-tab" class="col s12">
            <div id="new_assessment_modal" class="modal">
                <h5>Create a new assessment version</h5>
                <div class="modal-content">
                    <div class="input-field">
                        <input name="name" id="assessment-name">
                        <label for="assessment-name">Assessment version name</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn-flat waves-effect waves-light btn-card cancel modal-close">Cancel</button>
                    <button onclick="createAssessmentVersion()" class="btn waves-effect waves-teal btn-card">Create</button>
                </div>
            </div>

            <table class="highlight">
                <thead>
                <tr>
                    <td colspan="2">
                        <button data-target="new_assessment_modal"
                                class="btn grey lighten-3 black-text">
                            Create
                        </button>
                    </td>
                </tr>
                </thead>

                <%
                    final List<AssessmentVersion> assessmentVersions = Platform.instance().getAssessmentVersions().getAll();
                    for (AssessmentVersion a : assessmentVersions) {
                %>
                <tr>
                    <td class="nowrap-column">
                        <a href="<%=response.encodeURL("assessment-version.jsp?id=" + a.getId())%>"><%=a.getName()%></a>
                    </td>
                    <td width="100%">
                        <%=(a.isActivated())?"Activated":""%>
                    </td>
                </tr>
                <%
                    }
                %>


            </table>
        </div>
    </div>


    <%@include file="footer.jsp"%>
</body>
</html>
